# Enmu

## switch with enmu

```swift
enum AnimalType: String {
    case Mammal //No need of case Mammal = "Mammal"
    case Reptile
    case Fish
}

struct Animal {
    let name: String
    let type: AnimalType?
}


let lion = Animal(name: "Lion", type: .Mammal)

switch lion.type {

case .Mammal?:
    break
case .Reptile?:
    break
case .Fish?:
    break
case nil:
    break

}
```

## init from String

```swift
let atype: AnimalType = AnimalType(rawValue: "Mammal");
```
