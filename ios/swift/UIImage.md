# UIImage

1. PNG图片转UIImage

  `let image ＝ UIImage(named :"PNG图片名.png")`

2. UIImage转Data

  `let data ＝ UIImagePNGRepresentation( image )`

3. Data转UIImage

  `let image ＝ UIImage(data : data)`
