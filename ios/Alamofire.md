# iOS开发：在Swift中使用Alamofire发送HTTP请求

## 创建项目

1. 打开Xcode，点击Create a new Xcode project

2. 选择Single View App，点击Next

3. 输入Product Name：demo，Language选择Swift，点击Next

4. 选择一个目录存放你的项目

## 使用Pod安装Alamofire

1. 关掉Xcode

2. 在你的项目目录里，打开一个创建一个文件Podfile

3. 输入

    ```xml
    source 'https://github.com/CocoaPods/Specs.git'
    platform :ios, '10.0'
    use_frameworks!

    target 'demo' do
        pod 'Alamofire', '~> 4.7'
    end
    ```

4. 在终端中输入：`pod install`

5. 打开demo目录，双击demo.xcworkspace打开项目

## 使用Alamofire发送HTTP请求

1. 打开文件ViewController

2. 在viewDidLoad函数内调用Alamofire

    ```
    Alamofire.request("https://api.github.com/gists").responseJSON { response in
        print("Request: \(String(describing: response.request))")   // original url request
        print("Response: \(String(describing: response.response))") // http url response
        print("Result: \(response.result)")                         // response serialization result

        if let json = response.result.value {
            print("JSON: \(json)") // serialized json response
        }

        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
            print("Data: \(utf8Text)") // original server data as UTF8 string
        }
    }
    ```

3. ⌘+R启动APP
