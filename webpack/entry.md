## Single Entry

```javascript
const config = {
  entry: './path/to/my/entry/file.js'
};
```

## Object Syntax

```
const config = {
  entry: {
    app: './src/app.js',
    vendors: './src/vendors.js'
  }
};
```

