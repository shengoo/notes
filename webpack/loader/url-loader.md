```
{
    test: /\.(jpg|png|webp|gif|otf|ttf|woff|woff2|ani)$/,
    loader: `url-loader?name=[name]${hashFormat.file}.[ext]&limit=10000`
}
```