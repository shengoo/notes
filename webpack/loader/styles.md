# Loaders

## style-loader

`style-loader` adds CSS to the DOM by injecting a `style` tag.

## css-loader

The `css-loader` interprets `@import` and `url()` like `import/require()` and will resolve them.

## postcss-loader

Use it after `css-loader` and `style-loader`, but before other preprocessor loaders like e.g `sass|less|stylus-loader`, if you use any.

## Autoprefixer

Autoprefixer is a service for managing vendor prefixes. It adds missing prefixes and deletes obsolete ones.

## Extract CSS

Extract text from a bundle, or bundles, into a separate file.
Needed in production mode.

# Configuration

## Use in js:

```js
import from './file.css'
import from './file.less'
import from './file.sass'
```

## Development

```js
module.exports = {
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
							plugins: [
								autoprefixer({
									browsers: ['> 5%']
								})
							]
						}
                    }
                ]
            },
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
							plugins: [
								autoprefixer({
									browsers: ['> 5%']
								})
							]
						}
                    },
                    'less-loader'
                ]
            }
        ]
    }
}
```

## Production

```js
module.exports = {
    module: {
		rules: [
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
                        {
							loader: 'css-loader',
							options: {
								importLoaders: 1,
								minimize: true,
								sourceMap: true,
							},
						},
						{
							loader: 'postcss-loader',
							options: {
								plugins: [
									autoprefixer({
										browsers: ['> 5%']
									})
								]
							}
						},
					]
				})
			},
			{
				test: /\.less$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
                        {
							loader: 'css-loader',
							options: {
								importLoaders: 1,
								minimize: true,
								sourceMap: true,
							},
						},
						{
							loader: 'postcss-loader',
							options: {
								plugins: [
									autoprefixer({
										browsers: ['> 5%']
									})
								]
							}
						},
						'less-loader']
				})
			},
		]
	},

	plugins: [
		// Note: this won't work without ExtractTextPlugin.extract(..) in `loaders`.
		new ExtractTextPlugin({filename: '[name].[hash:8].css'})
	],
}

```
