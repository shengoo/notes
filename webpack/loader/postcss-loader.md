1. Simple

```js
{
    test: /\.css$/,
    use: [ 'style-loader', 'postcss-loader' ]
}
```

2. Recommended

```js
{
    test: /\.css$/,
    use: [
        'style-loader',
        { loader: 'css-loader', options: { importLoaders: 1 } },
        'postcss-loader'
    ]
}
```

3. Extract css

```js
const ExtractTextPlugin = require('extract-text-webpack-plugin')
    
module.exports = {
    module: {
        rules: [
        {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'postcss-loader'
                ]
            })
        }
        ]
    },
    plugins: [
            new ExtractTextPlugin('[name].css')
        ]
    }
```

4. Autoprefixer

```js

```
    
