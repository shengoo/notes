## Development

```js
plugins: [
    new HtmlWebpackPlugin({
        template: 'src/index.html',
        inject: true
    })
]
```


## production

```js
plugins: [
    new HtmlWebpackPlugin({
        template: 'src/index.html',
        inject: true,
        minify: {
            removeComments: true,
            collapseWhitespace: true,
            removeRedundantAttributes: true,
            useShortDoctype: true,
            removeEmptyAttributes: true,
            removeStyleLinkTypeAttributes: true,
            keepClosingSlash: true,
            minifyJS: true,
            minifyCSS: true,
            minifyURLs: true,
        }
    })
]
```