# Install TypeScript

> TypeScript is a typed superset of JavaScript that compiles to plain JavaScript.
>
> Any browser. Any host. Any OS. Open source.

```js
npm install -g typescript
```
