## Quick start

```bash
# Install the global CLI and its peer dependency
yarn global add tslint typescript

# Navigate to to your sources folder
cd path/to/project

# Generate a basic configuration file
tslint --init

# Lint TypeScript source globs
tslint -c tslint.json 'src/**/*.ts'
```
