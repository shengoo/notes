show dbs

use <db>

show collections

show users

show roles

show profile

show databases

## select

### not null

```
db.mycollection.find({ 'fieldname' : { $exists: true, $ne: null } });
```

# delete

```sql
db.getCollection('results').deleteMany({month: '201810'})
```

