# How to install mongodb

## Install MongoDB Community Edition with Homebrew

1. Update Homebrew’s package database.

```s
brew update
```

1. Install MongoDB.

```s
brew install mongodb
```

## Run MongoDB

1. To have launchd start mongodb now and restart at login:

```s
brew services start mongodb
```
