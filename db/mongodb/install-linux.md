# Install MongoDB

## Install

1. Import the public key used by the package management system.

    ```
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
	```
1. Create a list file for MongoDB.

	- Ubuntu 16.04

		```
		echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
		```

1. Reload local package database.

	```
	sudo apt-get update
	```

1. Install the MongoDB packages.

	```
	sudo apt-get install -y mongodb-org
	```

## Run MongoDB

1. Start MongoDB.

	```
	sudo service mongod start
	```

1. Verify that MongoDB has started successfully

	```
	service mongod status
	```

1. Stop MongoDB.

	```
	sudo service mongod stop
	```

1. Restart MongoDB.

	```
	sudo service mongod restart
	```

## auto start on boot

```
systemctl enable mongod.service
```

## Uninstall MongoDB Community Edition

1. Stop MongoDB.

	```
	sudo service mongod stop
	```

1. Remove Packages.

	```
	sudo apt-get purge mongodb-org*
	```

1. Remove Data Directories.

	```
	sudo rm -r /var/log/mongodb
	sudo rm -r /var/lib/mongodb
	```
