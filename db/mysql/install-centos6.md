# centos6 mysql

## install

1. Install MySQL and tell it which runlevels to start on:

```
sudo yum install mysql-server
sudo /sbin/chkconfig --levels 235 mysqld on
```

1. Then to start the MySQL server:

```
sudo service mysqld start
```

## Using MySQL

The standard tool for interacting with MySQL is the mysql client which installs with the mysql-server package. The MySQL client is used through a terminal.

### Root Login

To log in to MySQL as the Root User:

```
mysql -u root -p
```

### Create a New MySQL User and Database

```
create database testdb;
create user 'testuser'@'localhost' identified by 'password';
grant all on testdb.* to 'testuser' identified by 'password';
```

You can shorten this process by creating the user while assigning database permissions:

```
create database testdb;
grant all on testdb.* to 'testuser' identified by 'password';
```

2. Then exit MySQL.

```
exit
```

### Create a Sample Table

1. Log back in as testuser.

```
mysql -u testuser -p
```

2. Create a sample table called customers. This creates a table with a customer ID field of the type INT for integer (auto-incremented for new records, used as the primary key), as well as two fields for storing the customer’s name.

```
use testdb;
create table customers (customer_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, first_name TEXT, last_name TEXT);
```

3. Then exit MySQL.

```
exit
```

## Reset the MySQL Root Password

If you forget your root MySQL password, it can be flushed and then reset.

1. Stop the current MySQL server instance, then restart it with an option to not ask for a password.

```
sudo /etc/init.d/mysqld stop
sudo mysqld_safe --skip-grant-tables &
```

2. Reconnect to the MySQL server with the MySQL root account.

```
mysql -u root
```

3. Use the following commands to reset root’s password. Replace password with a strong password.

```
use mysql;
update user SET PASSWORD=PASSWORD("password") WHERE USER='root';
flush privileges;
exit
```

4. Then restart MySQL.

```
sudo service mysqld restart
```

You’ll now be able to log in again using mysql -u root -p.
