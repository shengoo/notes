# mysql commands

## login

```
mysql -u root -p
```

### logout

```
exit
```

## create database

```
CREATE DATABASE blog DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
```

## list db

```
SHOW DATABASES
```

## list user

```
select host, user, password from mysql.user;
```
