var mysql = require('mysql');
const bandwagon = require('./servers').bandwagon
var connection = mysql.createConnection(bandwagon);

connection.connect(function (err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }
    console.log('connected as id ' + connection.threadId);
});

connection.query('SELECT * from session', function (error, results, fields) {
    if (error) {
        console.log(error)
    }
    console.log('The solution is: ', results);
});

connection.end(function (err) {
    console.log('connection terminated');
});
