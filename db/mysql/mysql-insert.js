var mysql = require('mysql');
const bandwagon = require('./servers').bandwagon
var connection = mysql.createConnection(bandwagon);

connection.connect(function (err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }
    console.log('connected as id ' + connection.threadId);
});

connection.query('INSERT INTO session SET ?',
    {
        userid: 'test',
        token: 'test-token'
    },
    function (error, results, fields) {
        if (error) throw error;
        console.log(results.insertId);
    }
);

connection.end(function (err) {
    console.log('connection terminated');
});
