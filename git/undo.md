# git的后悔药，各种revert、reset、回退、回滚操作的方式

1. `git reset`

	撤销`git add`过的文件

1. `git checkout .`

	撤回没提交的更改，要在repo的根目录运行

1. `git reset --hard HEAD`

	也可以撤回没提交的更改，可以在任意的子目录中运行

1. `git clean -fdx`

	删除untracked文件

1. `git reset --hard origin/master`

	回到过去

- This will unstage all files you might have staged with git add:

    ```
    git reset
	```

- This will revert all local uncommitted changes (should be executed in repo root):

	```
	git checkout .
	```

- You can also revert uncommitted changes only to particular file or directory:

	```
	git checkout [some_dir|file.txt]
	```

- Yet another way to revert all uncommitted changes (longer to type, but works from any subdirectory):

	```
	git reset --hard HEAD
	```

- This will remove all local untracked files, so only git tracked files remain:

	```
	git clean -fdx
	```

git reset --hard origin/master
