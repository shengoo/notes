# 使用本地分支开发，将多个Commit合并为master上的一个Commit

最近git迁移到gerrit上，使用master开发。
而gerrit的代码审核要求只能提交一个Commit，这就需要我们每次提交的时候都用`git commit --amend`，



```
  1 Squashed commit of the following:
  2
  3 commit f4f53a3a1a02dd7824a0846434ccf30cdd1d08ba
  4 Author: shengqing <shengqing@babytree-inc.com>
  5 Date:   Wed Dec 27 16:29:57 2017 +0800
  6
  7     Changed a file.
  8
  9 commit 4e411e1c13205bc2d8cb150e0b3b01613530f1f0
 10 Author: shengqing <shengqing@babytree-inc.com>
 11 Date:   Wed Dec 27 16:29:31 2017 +0800
 12
 13     add a file
 14
 15 # Please enter the commit message for your changes. Lines starting
 16 # with '#' will be ignored, and an empty message aborts the commit.
 17 #
 18 # On branch master
 19 # Your branch is up-to-date with 'origin/master'.
 20 #
 21 # Changes to be committed:
 22 #   new file:   1.txt
 23 #
 ```