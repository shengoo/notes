set proxy

```
git config --global http.https://github.com.proxy socks5://127.0.0.1:1086
```

unset
```
git config --global --unset http.proxy
git config --global --unset http.https://github.com.proxy
```
