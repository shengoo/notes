server {
        listen 80;
        listen [::]:80;
        server_name www.xxx.com;
        return 301 https://www.xxx.com$request_uri;
}
server {
        listen 443;
        server_name www.xxx.com;
        ssl on;
        root /var/www/www.xxx.com;
        index index.php;
        ssl_certificate   cert/www.xxx.com.pem;
        ssl_certificate_key  cert/www.xxx.com.key;
        ssl_session_timeout 5m;
        ssl_ciphers xxx;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;

        location / {
                try_files $uri $uri/ /index.php?$args;
        }

        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        }
}
