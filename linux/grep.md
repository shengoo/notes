# grep

grep -rnw 'word to find'

- `-r` or `-R` is recursive.

- `-n` is line number

- `-w` match the whole word

- `-l` only show file name