
## 创建screen会话

可以先执行：screen -S lnmp ，screen就会创建一个名字为lnmp的会话。

## 暂时离开，保留screen会话中的任务或程序

当需要临时离开时（会话中的程序不会关闭，仍在运行）可以用快捷键Ctrl+a d(即按住Ctrl，依次再按a,d)

## 恢复screen会话

当回来时可以再执行执行：screen -r lnmp 即可恢复到离开前创建的lnmp会话的工作界面。如果忘记了，或者当时没有指定会话名，可以执行：screen -ls screen会列出当前存在的会话列表

## 关闭screen的会话

执行：exit ，会提示：[screen is terminating]，表示已经成功退出screen会话。

## kill all

`killall screen`
