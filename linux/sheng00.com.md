# Move sheng00.com

1. export xml from original wordpress site.

1. download wordpress installation file

    1. `cd /var/www`

    1. `wget https://cn.wordpress.org/wordpress-4.9.4-zh_CN.tar.gz`

1. unzip wordpress

    `tar xzvf latest.tar.gz`

1. rename folder

    `mv wordpress www.sheng00.com`

1. create wp-config.php file

    `cp /var/www/www.sheng00.com/wp-config-sample.php /var/www/www.sheng00.com/wp-config.php`

1. fill db information

1. create config file `vim /etc/nginx/sites-available/www.sheng00.com`

    ```js
    server {
        listen 80;
        listen [::]:80;
        server_name blog.sheng00.com;
        root /var/www/www.sheng00.com;
        index index.php;
        location / {
                try_files $uri $uri/ /index.php?$args; # support wordpress permlink
        }
        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        }
    }
    ```

1. install wordpress

1. login wordpress

1. add plugin from https://gist.github.com/shengoo/f195dc87e22ba50fe6601b1547e01535

    `vim /var/www/www.sheng00.com/wp-content/plugins/wordpress-importer-post-id-preservation.php`

1. enable plugin

1. import exported xml file

1. change dns address from old ip to new ip

1. change site url

1. change config file `vim /etc/nginx/sites-available/www.sheng00.com` server_name from blog.sheng00.com -> www.sheng00.com

1. install plugin `Velvet Blues Update URLs` to replace media urls from blog.sheng00.com -> www.sheng00.com
