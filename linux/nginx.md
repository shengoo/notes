## Install nginx and config php in Ubuntu

> Ubuntu

```
sudo apt-get update
sudo apt-get install nginx
```

## config

default config file:

```
/etc/nginx/sites-available/default
```

default www folder:

```
/var/www/html
```


## php

install php

```
sudo apt-get install php-fpm php-mysql
```

### config php

```
vim /etc/php/7.0/fpm/php.ini
```

### Configure Nginx to Use the PHP Processor

```
vim /etc/nginx/sites-available/default
```

/etc/nginx/sites-available/default:

```
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root /var/www/html;
    index index.php index.html index.htm index.nginx-debian.html;

    server_name server_domain_or_IP;

    location / {
        try_files $uri $uri/ =404;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
    }

    location ~ /\.ht {
        deny all;
    }
}
```

Test your configuration file for syntax errors by typing:

```
sudo nginx -t
```

reload nginx
```
nginx -s reload
```

or

```
sudo systemctl reload nginx
```

### Create a PHP File to Test Configuration

```
vim /var/www/html/info.php
```

/var/www/html/info.php:
```
<?php
phpinfo();
```

open http://server_domain_or_IP/info.php

### permission

```
sudo chown -R www-data /var/www/html
```
