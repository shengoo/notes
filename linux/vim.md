# Vim


## 

### 5.3有选择性的保存

1. 使用v选择文本

1. `:w TEST`会把选中的文本保存到TEST文件中

### 5.4 提取和合并文件

使用`:r TEST`将TEST文件中的内容提取进来

也可以使用`:r !ls`把ls命令的输出放置在光标下面

### 第五讲小结

1. `:!command`用于执行一个外部命令

    `:!ls` 显示当前目录的内容

1. `:w FILENAME`可讲当前VIM编辑的文件保存到名为FILENAME的文件中。

1. `:v motion :w FILENAME`可讲选中的内容保存到FILENAME中。

1. `:r FILENAME`可提取文件FILENAME并将其插入到当前文件对光标位置后面。

1. `:r !ls`可以将ls命令对出书放置在光标后面。

### 6.1 打开类命令

