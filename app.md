icon: #001935
brand: #36465d


## api

### dashboard

1. all

  ```
  var dashboardRequest = tmApiClient.dashboardRequest(nil, callback: { (response, error) in
      print("dashboard - \(response![AnyHashable("posts")])")
      self.posts = response!["posts"] as! NSArray
      self.view.hideToastActivity()
      print(self.posts.count);
      self.tableView.reloadData()
  })
  dashboardRequest.resume()
  ```

1. videos

  ```
  var dashboardRequest = tmApiClient.dashboardRequest(["type": "video"], callback: { (response, error) in
      print("dashboard - \(response![AnyHashable("posts")])")
      self.posts = response!["posts"] as! NSArray
      self.view.hideToastActivity()
      print(self.posts.count);
      self.tableView.reloadData()
  })
  dashboardRequest.resume()
  ```
