# odin app

## domain

联调环境域名为：
odinapp.dev.ke.com

测试环境域名为：
odinapp.off.ke.com

线上环境域名为：
odinapp.ke.com

## mysql

create db:

```sql
CREATE DATABASE odin_app DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
```

### table session

```sql
CREATE TABLE `session` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `token` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
```


## 首页

1. 首页列表
	
	列表
	
	详情

2. 搜索

	联想API

	历史记录

	tab

	列表

3. 核心看板

	详情页

4. TOP主题

	列表

	详情

5. 地图

## 报告

1. 分类

2. 分类列表

3. 报告详情

4. 分享

5. 报错

6. 搜索

7. 权限

## 指标

1. 列表
2. 详情
3. 分组

## 个人

1. 个人信息

	头像管理（后台提供）

	用户信息（ehr）

2. 帮助中心

	产品介绍（后台、H5）

	常见问题（后台、H5）

3. 问题反馈（后台）

## 需要的API

个人：

1. 消息列表
2. 问题反馈

指标

1. 分组的增删改查
2. 指标列表
	1. 根据分组查询
	2. 搜索查询
3. 指标详情

报告

1. 报告分类
2. 报告列表
3. 我的报告增删改查
4. 报告详情

## 可以先做的事

1. 接session
2. 上线API
3. 上线数据库
4. 企业级发布
5. ehr请求加签名

