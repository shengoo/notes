# uc api

## validate

### success response

```json
{
    "accountSystemId": "employee",
    "phone": "150****0378",
    "success": true,
    "userId": 1000000026025946
}
```

### error response

```json
{
    "code": "auth.user.illegal",
    "message": "用户名或密码不正确"
}
```
