Every app has a main entry point.

Angular has a powerful concept of modules. When you boot an Angular app, you’re not booting a component directly, but instead you create an NgModule which points to the component you want to load.

### app.module.ts
```ts
@NgModule({
  declarations: [
    // components
    AppComponent
], imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  // bootstrap component
  bootstrap: [AppComponent]
})
export class AppModule { }
```

The first thing we see is an `@NgModule` decorator. Like all decorators, this `@NgModule( ... )` code **adds metadata to the class immediately following** (in this case, AppModule).

Our @NgModule decorator has three keys: declarations, imports, providers, and bootstrap.

### declarations
declarations specifies the components that are defined in this module. 

### imports
imports describes which dependencies this module has. We’re creating a browser app, so we want to import the BrowserModule.

### providers
providers is used for dependency injection. So to make a service available to be injected throughout our application, we will add it here.

### bootstrap
bootstrap tells Angular that when this module is used to bootstrap an app, we need to load the AppComponent component as the top-level component.