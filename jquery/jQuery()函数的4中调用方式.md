# jQuery()函数的4中调用方式

## 选择元素

```js
$(selector)
$(selector,context)
```

## 封装成jQuery对象

```js
$(Element|Document|Window)
```

## 创建jQuery对象

```js
$('<img/>')
$('<img/>',{
    src: url
})
```

## 传入函数，在文档加载完毕运行

DOMContentLoaded

```js
jQuery(function(){});
$(document).ready(function(){});
```
