# React Native 简介

1. What is React Native
1. Why React Native
1. How to

## What

### Build native mobile apps using JavaScript and React

1. JavaScript code
1. CSS-like stylesheets and 
1. HTML-like tags for layout

### A React Native app is a real mobile app

With React Native, you don't build a "mobile web app", an "HTML5 app", or a "hybrid app".
View -> UIView(iOS) / View(Android)

### Don't waste time recompiling

Hot Reloading

### Use native code when you need to

```jsx
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { YourNativeComponent } from './your-native-code';

class SomethingFast extends Component {
  render() {
    return (
      <View>
        <YourNativeComponent />
      </View>
    );
  }
}
```

### Hot Update

## React and React Native

## Basics

```jsx
import React, { Component } from 'react';
import { Text, View } from 'react-native';

export default class HelloWorldApp extends Component {
  render() {
    return (
      <View>
        <Text>Hello world!</Text>
      </View>
    );
  }
}
```

## style

```jsx
import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  bigblue: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 30,
  },
  red: {
    color: 'red',
  },
});

export default class LotsOfStyles extends Component {
  render() {
    return (
      <View>
        <Text style={styles.red}>just red</Text>
        <Text style={styles.bigblue}>just bigblue</Text>
        <Text style={[styles.bigblue, styles.red]}>bigblue, then red</Text>
        <Text style={[styles.red, styles.bigblue]}>red, then bigblue</Text>
      </View>
    );
  }
}
```

## Height and Width

1. 不需要单位(~~px dp ...~~)
1. 逻辑像素，iPhone6 -> width: 375
1. 图片可以使用@2x，@3x
1. 1像素(1/750) : StyleSheet.hairlineWidth

## flex layout

和css里的flex几乎没有区别，flexDirection默认值column（竖着）

## Networking

iOS必须用https

1. fetch
2. XMLHttpRequest
3. WebSockets

```jsx
try {
    let response = await fetch(
        'https://facebook.github.io/react-native/movies.json',
    );
    let responseJson = await response.json();
    return responseJson.movies;
    } catch (error) {
    console.error(error);
}
```

## 指定平台的代码

1. Platform module

```jsx
if(Platform.OS === 'ios'){
    renderIOS()
} else {
    renderAndroid()
}
```

1. 通过文件后缀名

```js
component.ios.js
component.android.js
```

## 页面间跳转

1. React Navigation
2. React Native Navigation
3. Navigator(Legacy)

## 本地存储

AsyncStorage
iOS： dictionary或者文件
Android： RocksDB或者SQLite

```jsx
_storeData = async () => {
  try {
    await AsyncStorage.setItem('@MySuperStore:key', 'I like to save it.');
  } catch (error) {

  }
}

_retrieveData = async () => {
  try {
    const value = await AsyncStorage.getItem('TASKS');
    if (value !== null) {
      console.log(value);
    }
   } catch (error) {

   }
}
```

## debug

1. 开发菜单
iOS: ⌘D
Android: ⌘M

## Blueprint to becoming a React Native Developer

1. Learn JavaScript
1. Learn React
1. Learn React Native
1. Learn a Backend
1. Learn Redux
