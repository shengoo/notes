# react navigation

## header config

title

```js
static navigationOptions = {
    title: 'Home',
};
// or
static navigationOptions = ({navigation}) => ({
    title: 'Home',
});
```

header right

```js
static navigationOptions = ({navigation}) => ({
    title: 'Home',
    headerRight: (
        <SearchButton onPress={() => {
            navigation.navigate('Search');
        }}/>
    ),
});
```
