# Environment setup

## install 

> For Mac os

1. Install XCode from App Store
1. Install Homebrew: `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
1. Install Node: `brew install node`
1. Install watchman: `brew install watchman`

    Watchman is a tool by Facebook for watching changes in the filesystem. It is highly recommended you install it for better performance.

1. Install React Native CLI: `npm install -g react-native-cli`

## Creating a new application

1. `react-native init AwesomeProject`

## Running your React Native application

```shell
cd AwesomeProject
react-native run-ios
```
