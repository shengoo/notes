import React from 'react';
import {
    View,
} from 'react-native'
import { connect } from "react-redux";

class Example extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: 'Example',
    });

    render() {
        return (
            <View>

            </View>
        );
    }
}
const stateToProps = state => {
    return {

    }
};

const dispatchToProps = (dispatch) => ({

});

export default connect(stateToProps, dispatchToProps)(Example)
