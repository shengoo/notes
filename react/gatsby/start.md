# 使用Gatsby生成静态网站并部署在GitHub上

## 什么是Gatsby

> Blazing-fast static site generator for React

[Gatsby](https://www.gatsbyjs.org/)是一个基于React极其快的静态网站生成工具

支持各种数据源，markdown、Wordpress等

## 创建网站

1. 安装Gatsby命令行工具：

    ```shell
    npm install --global gatsby-cli
    ```

1. 创建一个新的网站

    ```shell
    gatsby new sheng00.cn
    ```

1. 运行刚才创建的网站

    ```shell
    cd sheng00.cn
    gatsby develop
    ```

    打开localhost:8000即可看到

## 部署在GitHub上

1. 在GitHub上创建一个repository

1. 在刚才生成的网站运行下面的命令

    ```shell
    git add -A
    git commit -m "first commit"
    git remote add origin git@github.com:shengoo/sheng00.cn.git
    git push -u origin master
    ```

1. 安装gh-pages

    ```shell
    yarn add gh-pages
    ```

1. 在package.json里增加一个脚本

    ```json
    "deploy": "gatsby build --prefix-paths && gh-pages -d public"
    ```

1. 部署到GitHub

    ```shell
    yarn deploy
    ```

## 设置自定义域名

1. 从域名注册商那里，把域名指向`yourusername.github.io`

1. 在GitHub的repository的设置里，设置Custom Domain：www.sheng00.cn

1. 在Gatsby生成的网站中，新建一个目录`static`，创建一个文件`CNAME`

    ```js
    www.sheng00.cn
    ```

1. 再次部署

    ```js
    yarn deploy
    ```

源代码网址：https://github.com/shengoo/sheng00.cn