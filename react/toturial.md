# React

React is a declarative, efficient, and flexible JavaScript library for building user interfaces.

* Declarative

    Declarative views make your code more predictable and easier to debug.

* Component-Based

    Build encapsulated components that manage their own state, then compose them to make complex UIs.

* Learn Once, Write Anywhere

    We don’t make assumptions about the rest of your technology stack, so you can develop new features in React without rewriting existing code.

    React can also render on the server using Node and power mobile apps using React Native.

## JSX

`const element = <h1>Hello, world!</h1>;`

JSX produces React “elements”.

### Embedding Expressions in JSX

```jsx
function formatName(user) {
  return user.firstName + ' ' + user.lastName;
}

const user = {
  firstName: 'Harper',
  lastName: 'Perez'
};

const element = (
  <h1>
    Hello, {formatName(user)}!
  </h1>
);
```

### JSX is an Expression Too

```jsx
function getGreeting(user) {
  if (user) {
    return <h1>Hello, {formatName(user)}!</h1>;
  }
  return <h1>Hello, Stranger.</h1>;
}
```

### Specifying Attributes with JSX
`const element = <div tabIndex="0"></div>;`
`const element = <img src={user.avatarUrl}></img>;`

## Rendering Elements

```jsx
const element = <h1>Hello, world</h1>;
ReactDOM.render(
  element,
  document.getElementById('root')
);
```

### Updating the Rendered Element

React elements are immutable. Once you create an element, you can’t change its children or attributes. An element is like a single frame in a movie: it represents the UI at a certain point in time.

### React Only Updates What’s Necessary

React DOM compares the element and its children to the previous one, and only applies the DOM updates necessary to bring the DOM to the desired state.

## Components

> Components let you split the UI into independent, reusable pieces, and think about each piece in isolation.

## Props
