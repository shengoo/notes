# add redux-saga in your app

## Sagas



1. create a file `configureStore.js`

    ```js
    import {createStore, applyMiddleware, compose} from 'redux';
    import api from './middleware/api';
    import logger from 'redux-logger';
    import reducers from './reducers';

    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const middlewares = [api];

    if (process.env.NODE_ENV === `development`) {// only show logs in development environment.
        middlewares.push(logger);
    }

    export default function configureStore(preloadedState) {
        return createStore(
            reducers,
            preloadedState,
            composeEnhancers(applyMiddleware(...middlewares))
        )
    }
    ```
