# Three Rules for Structuring (Redux) Applications

## Rule #1: Organize by feature

Redux + React:

```
actions/
  todos.js
components/
  todos/
    TodoItem.js
    ...
constants/
  actionTypes.js
reducers/
  todos.js
index.js
rootReducer.js
```

AngularJS:

```
controllers/
directives/
services/
templates/
index.js
```

Ruby on Rails:

```
app/
  controllers/
  models/
  views/
```

It may seem reasonable to group similar objects together like this (controllers with controllers, components with components), however as the application grows this structure does not scale.

When you add and change features, you’ll start to notice that some groups of objects tend to change together. **These objects group together to form a feature module.** For example, in a todo app, when you change the `reducers/todos.js` file, it is likely that you will also change `actions/todos.js` and `components/todos/*.js`.

Instead of wasting time scrolling through your directories looking for todos related files, it is much better to have them sitting in the same location.

**A better way to structure Redux + React project:**

```
todos/
  components/
  actions.js
  actionTypes.js
  constants.js
  index.js
  reducer.js
index.js
rootReducer.js
```

## Rule #2: Create strict module boundaries

As an example, let’s say that we want to add a new feature to our TODO app: We want the ability to manage TODO lists by project. That means we will create a new module called projects.

```
projects/
  components/
  actions.js
  actionTypes.js
  reducers.js
  index.js
todos/
index.js
```

Now, it is obvious that the projects module will have a dependency on todos. In this situation, it is important that we exercise discipline and only couple to the “public” API exposed in todos/index.js.

**BAD**

```javascript
import actions from '../todos/actions';
import TodoItem from '../todos/components/TodoItem';
```

**GOOD**

```javascript
import todos from '../todos';
const { actions, TodoItem } = todos;
```

**BAD**

```javascript
const ProjectTodos = ({ todos }) => (
  <div>
    {todos.map(t => <TodoItem todo={t}/>)}
  </div>
);

// Connect to todos state
const ProjectTodosContainer = connect(
  // state is Redux state, props is React component props.
  (state, props) => {
    const project = state.projects[props.projectID];

    // This couples to the todos state. BAD!
    const todos = state.todos.filter(
      t => project.todoIDs.includes(t.id)
    );

    return { todos };
  }
)(ProjectTodos);
```

**GOOD**

```javascript
import { createSelector } from 'reselect';
import todos from '../todos';

// Same as before
const ProjectTodos = ({ todos }) => (
  <div>
    {todos.map(t => <TodoItem todo={t}/>)}
  </div>
);

const ProjectTodosContainer = connect(
  createSelector(
    (state, props) => state.projects[props.projectID],

    // Let the todos module provide the implementation of the selector.
    // GOOD!
    todos.selectors.getAll,

    // Combine previous selectors, and provides final props.
    (project, todos) => {
      return {
        todos: todos.filter(t => project.todoIDs.includes(t.id))
      };
    }
  )
)(ProjectTodos);
```

## Rule #3: Avoid circular dependencies

Circular dependency example
Given:

a.js

```javascript
import b from './b';

export const name = 'Alice';

export default () => console.log(b);
```

b.js

```js
import { name } from './a';

export default `Hello ${name}!`;
```

What happens with the following code?

```js
import a from './a';

a(); // ???
```
