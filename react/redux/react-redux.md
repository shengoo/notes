## <Provider store>

### example

#### React
```js
ReactDOM.render(
  <Provider store={store}>
    <MyRootComponent />
  </Provider>,
  rootEl
)
```

#### React Router
```js
ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}>
        <Route path="foo" component={Foo}/>
        <Route path="bar" component={Bar}/>
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)
```


## connect([mapStateToProps], [mapDispatchToProps], [mergeProps], [options])

* `mapStateToProps(state, [ownProps]): stateProps`
* `mapDispatchToProps(dispatch, [ownProps]): dispatchProps`
* `mergeProps(stateProps, dispatchProps, ownProps): props`
* `options`
    * `pure = true`
    * `withRef = false`

### Example

#### 只注入 dispatch，不监听 store
```js
export default connect()(TodoApp)
```
#### 注入全部没有订阅 store 的 action creators (addTodo, completeTodo, ...)
```js
import * as actionCreators from './actionCreators'

export default connect(null, actionCreators)(TodoApp)
```

#### 注入 dispatch 和全局 state
> 不要这样做！这会导致每次 action 都触发整个 TodoApp 重新渲染，你做的所有性能优化都将付之东流。
> 
> 最好在多个组件上使用 connect()，每个组件只监听它所关联的部分 state。
```js
export default connect(state => state)(TodoApp)
```