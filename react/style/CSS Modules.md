# 在react应用中使用模块化CSS

## 什么是模块化CSS?

> 模块化管理CSS，避免全局污染，实现模块化、可服用。

## 在react中应用

### 启用CSS Modules

在`css-loader`中增加一个选项：

```js
{
    loader: require.resolve('css-loader'),
    options: {
        importLoaders: 1,
        minimize: true,
        sourceMap: shouldUseSourceMap,
        modules: true, // 启用CSS Modules
    },
},
```

### CSS文件

app.css:

```css
.title{
    color: red;
}
```

最后会编译成：

```css
._2L1SLeGPg5sisdRmqO9mCH {
  color: red;
}
```

### JavaScript文件

app.js:

```js
import styles from './app.css';

class App extends Component {
  render() {
    return <div className={styles.title}>Hello World.</div>;
  }
}
```

最后会编译成：

```html
<div class="_2L1SLeGPg5sisdRmqO9mCH">Hello World.</div>
```