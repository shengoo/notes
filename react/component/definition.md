# Componet Definitions

## 使用ES6的类定义组件(Class Component)

```js
import React from 'react';

class ComponentA extends React.Component{
    render(){
        return <div>Hello {this.props.name} from ComponentA</div>
    }
}

export default ComponentA;
```

## 无状态的函数式声明组件(Stateless Function Components)

Many times, with presentational components, you don’t need the lifecycle methods and your component just needs a render() method. Such components can be defined as pure functions which are called as Stateless Functional Components in React.

在一个组件没有状态，并且不需要生命周期方法，只需要render的时候，可以选择使用函数式声明

```js
import React from 'react';

// es5
var ComponentB = function(props){
    return <div>Hello {props.name} from ComponentB</div>;
}

// es6
const ComponentB = (props)=>(
    <div>Hello {props.name} from ComponentB</div>
)

// consume only specific named properties from the props object, you can use destructing assignment syntax of ES6 like
const ComponentB = ({name})=>(
    <div>Hello {name} from ComponentB</div>
)

// es6 anonymous function
export default (props)=>(
    <div>Hello {props.name} from ComponentB</div>
);
```