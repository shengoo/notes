# React Component lifecycle

## Single component

### init

1. constructor
2. componentWillMount
3. render
4. componentDidMount

### update

1. componentWillReceiveProps
2. componentWillUpdate
3. render
4. componentDidUpdate

## with child

### init

1. constructor in parent
2. componentWillMount in parent
3. render in parent
3. constructor in child
4. componentWillMount in child 
5. render in child
5. componentDidMount in child 
6. componentDidMount in parent

### update

1. componentWillReceiveProps in parent
2. componentWillUpdate in parent
1. render in parent
3. componentWillReceiveProps in child
4. componentWillUpdate in child
1. render in child
5. componentDidUpdate in child
6. componentDidUpdate in parent

### unmount
1. componentWillUnmount in parent
1. componentWillUnmount in child