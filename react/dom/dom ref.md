## When to Use Refs

There are a few good use cases for refs:

Managing focus, text selection, or media playback.
Triggering imperative animations.
Integrating with third-party DOM libraries.
Avoid using refs for anything that can be done declaratively.

For example, instead of exposing open() and close() methods on a Dialog component, pass an isOpen prop to it.

You can: 
1. Add to a dom element.
2. Add to a class component
3. Add to a dom or component inside child.

```js
import React from 'react';

import DynamicTag from './DynamicTag';

class DomRef extends React.Component{

    // Accessible after mount
    componentDidMount(){
        console.log(this.textInput);
        console.log(this.dynamicTag);
        console.log(this.inputElement);
    }

    render(){
        return(
            <div>
                // add to dom
                <input type="text"
                    ref={(input) => { this.textInput = input; }}/>
                // add to a class component
                <DynamicTag tagName="span"
                    ref={(dt) => { this.dynamicTag = dt; }}/>
                // add inside child component
                <CustomTextInput
                    inputRef={el => this.inputElement = el}
                />
            </div>
        )
    }
}

function CustomTextInput(props) {
    return (
        <div>
            // will call function defined in parent
            <input ref={props.inputRef} />
        </div>
    );
}

export default DomRef;
```