```js
import React from 'react';

class DynamicTag extends React.Component{
    render(){
        // Must be capitalized.
        const Tag = this.props.tagName;
        return <Tag>Hello DynamicTag</Tag>;
    }
}

export default DynamicTag;
```