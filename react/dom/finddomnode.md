react-dom provide a method `fondDOMNode()` to get the DOM of component

cannot used on stateless components

```js
import { findDOMNode } from 'react-dom';

// Inside Component class
componentDidMound() {
  const el = findDOMNode(this);
}
```