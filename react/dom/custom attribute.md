```js
import React from 'react';
import ReactDOM from 'react-dom';

class CustomAttr extends React.Component {

    componentDidMount() {
        var element = ReactDOM.findDOMNode(this);
        element.setAttribute('size', 'big');
    }

    render() {
        return (
            <div>Custom attrbute.</div>
        )
    }
}

export default CustomAttr;
```