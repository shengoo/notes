# 通过修改react-scripts来自定义create-react-app的模板

[`create-react-app`](https://github.com/facebook/create-react-app)是一个无需任何配置就能轻松创建`react`应用使用的命令行工具，它主要是使用`react-scripts`来配置需要的`webpack`、`babel`等一系列工具。

`react-scripts`创建的应用可以满足大部分的需求，但是有时候我们需要修改或者创建自己的配置项。`react-scripts`提供了一个命令`eject`，使用`eject`命令可以将`react-scripts`内置的各种配置项暴露出来，这时候我们就可以通过更改配置文件。

[`eject`](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#npm-run-eject)可以让你自定义所有配置项。但是如果有很多相似的项目，建议fork一份`react-scripts`和其他需要的packges，做一个自己的模板使用。

## 自定义create-react-app的模板

### Fork [create-react-app](https://github.com/facebook/create-react-app)

打开[create-react-app](https://github.com/facebook/create-react-app)，fork出自己的一份`create-react-app`

建议fork一个稳定的分支，master不是稳定的。

在`packages`目录里，有一个目录`react-scripts`。`react-scripts`这个目录里包含了`build`、`test`、`start`你的react app的脚本。

### 修改配置

把我们fork好的`create-react-app` clone到本地，checkout一个稳定版的tag，打开`react-scripts/scripts/init.js`这个文件。

![add message](assets/add-message.jpg?raw=true)

在里面加一行`console.log('Hello world.');`

### 把我们自定义的`react-scripts`发布到NPM

先把`react-scripts`目录下的`package.json`文件里的`name`等字段的值改成我们自己的。

![change name](assets/change-name.jpg?raw=true)

现在从命令行切换到`react-scripts`目录里，使用`npm publish`命令行进行发布。如果npm提示你登陆，就登陆上自己的npm账号。

![npm published](assets/npm-published.jpg?raw=true)

### 测试我们自己的`react-scripts`

在命令行中使用我们自己的模板创建一个react-app

```shell
create-react-app test-app --scripts-version shengoo-react-scripts
```

![test my](assets/test-my.jpg?raw=true)

可以看到，我们的增加的那行`console.log`生效了。

接下来就可以通过修改`react-scripts`增加我们自己需要的功能了。

