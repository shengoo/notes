1. Install V8Js PHP extension

2. Make sure your React app takes its initial input as props

3. Set up a router that uses HTML5's window.history.pushState()

4. Redirect all PHP traffic within your application so it all goes through a single entry point (front controller pattern)

5. Fetch the data needed for a given route

6. Render your React components in PHP using V8Js and React's renderToString()

7. Render your component's bootstrap using the server-fetched data

