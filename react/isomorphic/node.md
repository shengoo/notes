## 使用`create-react-app`创建一个react的app

```
npx create-react-app my-app
cd my-app
npm start
```

## 增加前端路由功能

1. 安装redux react-redux react-router react-router-dom react-router-redux

    ```shell
    yarn add redux react-redux react-router react-router-dom react-router-redux
    ```

1. 使用react-router，编辑App.js文件

    ```js
    import React from 'react';
    import './App.css';

    import { connect, Provider } from 'react-redux'
    import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux'

    import { createStore, applyMiddleware } from 'redux'
    import createHistory from 'history/createBrowserHistory'

    import { Link } from 'react-router-dom'
    import { Route, Switch } from 'react-router'

    const history = createHistory()

    const store = createStore(
        routerReducer,
        applyMiddleware(routerMiddleware(history)),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    )

    const ConnectedSwitch = connect(state => ({
        location: state.location
    }))(Switch)

    const AppContainer = () => (
        <ConnectedSwitch>
            <Route exact path="/" component={() => (<h1>Home <Link to="/about">About</Link></h1>)} />
            <Route path="/about" component={() => (<h1>About <Link to="/">Home</Link></h1>)} />
        </ConnectedSwitch>
    );

    const App = connect(state => ({
        location: state.location,
    }))(AppContainer)

    export default () => (
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <App />
            </ConnectedRouter>
        </Provider>
    )
    ```