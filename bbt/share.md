# 分享相关

## 默认分享设置

title:

content:

url:

image: http://img01.babytreeimg.com/img/mg/dist/project/wetime/img/logo.png

## 微信分享

```html
<script src="//res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
<script>
    var sign = JSON.parse('<?php echo $_wx_sign_package; ?>');
    sign.jsApiList = ['onMenuShareTimeline','onMenuShareAppMessage','onMenuShareQQ','onMenuShareWeibo','onMenuShareQZone'];
    wx.config(sign);
    wx.ready(() => {
        // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
        var opt = {
            title: '限时领取价值千元世界获奖电子绘本',
            desc: '让世界获奖绘本帮孩子爱上阅读。',
            link: location.href,
            imgUrl: 'https://img01.babytreeimg.com/img/mg/dist/project/wetime/img/logo.png',
        };
        wx.onMenuShareAppMessage(opt);
        wx.onMenuShareTimeline(opt);
        wx.onMenuShareQQ(opt);
        wx.onMenuShareWeibo(opt);
        wx.onMenuShareQZone(opt);
    });
    wx.error(function(res){
        // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
        console.log(res);
    });
</script>
```

## QQ空间

```html
<script src="//qzonestyle.gtimg.cn/qzone/qzact/common/share/share.js"></script>
<script>
      // QQ空间分享设置
      setShareInfo({
        title:          '限时领取价值千元世界获奖电子绘本', // 分享标题
        summary:        '让世界获奖绘本帮孩子爱上阅读。', // 分享内容
        pic:            'https://img01.babytreeimg.com/img/mg/dist/project/wetime/img/logo.png', // 分享图片
        url:            location.href, // 分享链接
      });
    </script>
```