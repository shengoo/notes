# urls

## Controller and View

> `c`: controller name
>
> `a`: action name
>
> base url : http://www.babytree.com/ 

| url    | repo   | controller path |
| -      |-      |-          |-              |
| promo_web/{c}/{a} | Preg_promo | Babytree/Controller/Web/{c}Controller.php |
| wetime/promo/{c}/{a} | wetime-promo | Babytree/Controllers/Web/{c}Controller.php|
| admin/lama_app/{c}/{a} | wetime-admin |  | 
| wetime/web/{c}/{a} | wetime |  |

## assets

根目录：

线上：http://static02.babytreeimg.com/img/

开发：http://static02-shengqing.babytree-dev.com/img/

| 线上地址（现对于上面的目录） | repo | path in repo |
| - | - | - |
| static/wetime_promo_static/ | wetime-promo | baby/static |
| / | baby | static |



favicon: 
![](http://static02.babytreeimg.com/img/common/favicon.ico)

http://pic06.babytreeimg.com/2018/0416/FvI1956gIUdL3NDJ2JJ81ry-HPxJ_b.jpg

default avatar:

![](http://pic04.babytreeimg.com/img/common/136x136.png)

1. http://pic04-shengqing.babytree-dev.com/img/common/136x136.png
1. http://static02.babytreeimg.com/img/common/136x136.png
1. http://pic04.babytreeimg.com/img/common/136x136.png

默认宝宝头像：

![](https://videoplayer.babytreeimg.com/lama_baby_defualt_icon-1chhv8f6m1gm7hnk80j3je1ktr9.png)

default background:

1. http://pic02-20.babytree-test.com/foto3/photos/2016/0314/95/7/c6e2e6f2a2ded4287c9695c_b.png?1478075557
2. http://pic02.babytreeimg.com/foto3/photos/2016/0314/95/7/c6e2e6f2a2ded4287c9695c_b.png?1478075557

![](http://pic02.babytreeimg.com/foto3/photos/2016/0314/95/7/c6e2e6f2a2ded4287c9695c_b.png?1478075557)

下载链接：

1. 小时光：http://r.babytree.com/5d3rhhO
1. 小时光pro：https://r.babytree.com/ftzYLRS



logo: 

![小时光logo](http://img01.babytreeimg.com/img/mg/dist/project/wetime/img/logo.png)

http://img01.babytreeimg.com/img/mg/dist/project/wetime/img/logo.png
https://pic05.babytreeimg.com/img/mg/dist/project/wetime/img/logo-300.png


