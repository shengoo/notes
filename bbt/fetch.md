
```js
fetch(url)
    .then(res => res.json())
    .then(
        (result) => {
            if (result.status === 'success') {
                var data = result.data;
                this.setState({
                    isLoaded: true,
                    items: result.items
                });
            } else {
                Toast.show(result.message);
            }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
            this.setState({
                isLoaded: true,
                error
            });
        }
    );
```
