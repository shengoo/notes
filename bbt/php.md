# PHP脚本

## 常用

1. `var data = '<?= $data ?>';`

1. `var data = '<?php echo $data ?>';`

1. 输出json

  `var data = '<?= json_encode($tasks) ?>';`

1. domain

    ```php
    <?php echo \Chaos\Util\DomainUtil::getHttpDomain().\Chaos\Util\DomainUtil::getWebDomain(); ?>
    // http://shengqing.babytree-dev.com
    ```
1. add css

    ```php
    <?php
        \Chaos\Util\StaticFileControl::add('/img/wetime_promo_static/mg/project/download_app/css/style.css');
    ?>
    ```

1. add js

    ```php
    <?php
        \Chaos\Util\StaticFileControl::add('/img/bca/tracking/0.1.7/tracking.min.js');
    ?>
    ```

## 常用的

1. deeplink

    ```php
    <?php
        \Chaos\Util\StaticFileControl::add('/img/bca/deeplink/0.1.2/deeplink.min.js');
    ?>
    ```

1. tracking

    ```php
    <?php
        \Chaos\Util\StaticFileControl::add('/img/bca/tracking/0.1.7/tracking.min.js');
    ?>
    ```
