# eject

1. favicon: `vim public/index.html`

    ```html
    <link rel="icon" href="//static02.babytreeimg.com/img/common/favicon.ico" type="image/x-icon" />
    ```

1. viewport

    ```html
    user-scalable=0, maximum-scale=1, minimum-scale=1,
    ```

1. add css before </head>

    ```js
    <% if (!htmlWebpackPlugin.options.inject) { %>
        <% for (var css in htmlWebpackPlugin.files.css) { %>
            <?php \Chaos\Util\StaticFileControl::add('<%= htmlWebpackPlugin.files.css[css] %>'); ?>
        <% } %>
    <% } %>
    ```

1. add js before </body>

    ```js
    <% if (!htmlWebpackPlugin.options.inject) { %>
        <% for (var chunk in htmlWebpackPlugin.files.chunks) { %>
            <?php \Chaos\Util\StaticFileControl::add('<%= htmlWebpackPlugin.files.chunks[chunk].entry %>'); ?>
        <% } %>
    <% } %>
    ```

1. add method in paths file: `vim config/paths.js`

    ```js
    const getName = appPackageJson => require(appPackageJson).name;
    ```

1. config paths: `vim config/paths.js`

    ```js
    resolveApp('build') -> resolveApp('../../../baby/static/app/' + getName(resolveApp('package.json')))
    appFilename: resolveApp('../../../baby/tmpl/web/' + getName(resolveApp('package.json')) + '/index_tpl.php'),
    servedPath: '/static/wetime_promo_static/app/' + getName(resolveApp('package.json')) + '/',
    ```

1. HtmlWebpackPlugin: `vim config/webpack.config.prod.js`

    ```js
    inject: false,
    filename: paths.appFilename,
    ```

1. add ios8 support

    ```js
    'iOS >= 8',
    'Safari >= 8',
    ```

1. add less loader

    ```js
    {
        test: /\.less$/,
        loader: ExtractTextPlugin.extract(
            Object.assign(
                {
                    fallback: {
                        loader: require.resolve('style-loader'),
                        options: {
                            hmr: false,
                        },
                    },
                    use: [
                        {
                            loader: require.resolve('css-loader'),
                            options: {
                                importLoaders: 1,
                                minimize: true,
                                sourceMap: shouldUseSourceMap,
                            },
                        },
                        {
                            loader: require.resolve('postcss-loader'),
                            options: {
                                ident: 'postcss',
                                plugins: () => [
                                    require('postcss-flexbugs-fixes'),
                                    autoprefixer({
                                        browsers: [
                                            '>1%',
                                            'last 4 versions',
                                            'Firefox ESR',
                                            'not ie < 9',
                                            'iOS >= 8',
                                            'Safari >= 8',
                                        ],
                                        flexbox: 'no-2009',
                                    }),
                                ],
                            },
                        },
                        {
                            loader: 'less-loader',
                            options: {
                                javascriptEnabled: true,
                            },
                        },
                    ],
                },
                extractTextPluginOptions
            )
        ),
    },
    ```

## install bbt things

1. native.js

    1. script

        ```html
        <script src="//static02.babytreeimg.com/img/bca/native/0.1.4/native.min.js"></script>
        ```

    1. npm

        ```shell
        npm i git+ssh://git@192.168.24.32:front-end/native.git
        ```

1. tracking

    1. script

        ```html

        ```

    1. npm

        ```shell
        npm i git+ssh://git@192.168.24.32:front-end/tracking.git
        ```