const path = require('path')
const fs = require('fs')

// 开始计时
console.time('Total time')
// load数据
const data = require('./xueyuan2.json')

console.log(data.length) // 24121

// 单点
const singles = data.filter(item => !item.parent_tables && !item.child_tables)
console.log(singles.length) // 8970

// 先把单点的放进结果中
const result = singles.map(item => [item])
// const result = []

// 除了单点之外的其他
const relates = data.filter(item => item.parent_tables || item.child_tables)
console.log(relates.length) // 15151

// 当前处理的一些点
let current = [];
// 正在循环里的元素
const inloop = [];
relates.forEach(item => {
    // 看看这个元素有没有处理过
    if (result.some(any => any.includes(item))) {
        return;
    }
    current.push(item);
    inloop.push(item)
    // relates.splice(relates.indexOf(item))
    addRelatives(item)
    inloop.length = 0
    result.push(current)
    current = []
    console.log(result.length)
})

function addRelatives(node) {
    if (node.parent_tables) {
        addRelativesArray(node.parent_tables)
    }
    if (node.child_tables) {
        addRelativesArray(node.child_tables)
    }
}

function addRelativesArray(tables) {
    const nodes = relates.filter(item => tables.includes(item.table))
    nodes.forEach(item => {
        if (!inloop.includes(item)) {
            inloop.push(item)
            current.push(item)
            addRelatives(item)
        }
    })
}

result.sort((a, b) => (a.length - b.length))
const resultCount = result.reduce((pre, cur) => {
    const length = cur.length
    pre[length] = pre[length] ? pre[length] + 1 : 1;
    return pre
}, {})

// 把结果写入文件
const filename = path.resolve(__dirname, 'result', `resultgroup${new Date().valueOf()}.json`)
fs.writeFileSync(filename, JSON.stringify(result, null, 4), 'utf8');
const filename2 = path.resolve(__dirname, 'result', `resultgroupcount${new Date().valueOf()}.json`)
fs.writeFileSync(filename2, JSON.stringify(resultCount, null, 4), 'utf8');

console.timeEnd('Total time') // 21524.543ms
