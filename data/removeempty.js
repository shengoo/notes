const path = require('path')
const fs = require('fs')

console.time('Total time')
const data = require('./xueyuan.json')
console.log(data.length) // 24121

data.forEach(item => {
    if(item.parent_tables){
        item.parent_tables = item.parent_tables.filter(table => table !== '')
    }
    if(item.child_tables){
        item.child_tables = item.child_tables.filter(table => table !== '')
    }
})
console.log(data)


const filename = path.resolve(__dirname, `xueyuan2.json`)
// fs.writeFileSync(filename, JSON.stringify(data, null, 4), 'utf8');
console.timeEnd('Total time')
