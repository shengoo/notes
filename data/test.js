const data = require('./xueyuan.json')
console.log(data.length)

const parents = data.reduce((pre,current) => {
    if(current.parent_tables){
        const parent = current.parent_tables.filter(item => item !== '')
        pre = pre.concat(parent)
    }
    return pre;
}, [])

console.log(parents.length)

const uniqueParents = removeDuplicates(parents)
console.log(uniqueParents.length)

const children = data.reduce((pre,current) => {
    if(current.child_tables){
        current.child_tables.filter(item => item !== '').forEach(item => {
            if(pre.indexOf(item) === -1){
                pre.push(item)
            }
        })
    }
    return pre;
}, [])
console.log(children.length)

const all = data.map(item => item.table).concat(uniqueParents)
console.log(all.length)

const uniqueAll = removeDuplicates(all)

console.log(uniqueAll.length)


function removeDuplicates(arr){
    let unique_array = arr.filter(function(elem, index, self) {
        return index == self.indexOf(elem);
    });
    return unique_array
}
