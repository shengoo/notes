const path = require('path')
const fs = require('fs')

// 开始计时
console.time('Total time')
// load数据
const data = require('./group.json')
console.log(data.reduce((pre,cur) => {
    return pre + cur.length
}, 0)) // 24121

console.log(data.length) // 12660

result = data.map(arr => level(arr))
console.log(result.reduce((pre,cur) => {
    return pre + cur.length
}, 0)) // 24120
// 把结果写入文件
const filename = path.resolve(__dirname, 'result', `group-level${new Date().valueOf()}.json`)
fs.writeFileSync(filename, JSON.stringify(result, null, 4), 'utf8');

console.timeEnd('Total time') // 61971.811ms

function level(group) {
    const groupResult = [];
    const loop = []
    group.forEach(element => {
        let currentElement = groupResult.find(any => any.table === element.table) || {};
        if (!currentElement.table) {
            currentElement = { ...element, level: 1 }
            // console.log(1,currentElement)
            groupResult.push(currentElement)
        }
        if (currentElement.parent_tables) {
            // 遍历上游
            currentElement.parent_tables.forEach(item => {
                if (item === currentElement.table) {
                    return;
                }
                let find = groupResult.find(any => any.table === item)
                // 上游元素不在结果中
                if (!find) {
                    // 插入上游元素
                    find = group.find(any => any.table === item)
                    if (find) {
                        let parent = { ...find, level: 1, }
                        // console.log(2, parent)
                        groupResult.push(parent)
                    } else {
                        console.log(`cannot find parent ${item}`)
                    }
                } else {
                    if (find.level + 1 < currentElement.level)
                        currentElement.level = find.level + 1;
                }
            })
        }
        if (currentElement.child_tables) {
            currentElement.child_tables.forEach(item => {
                if (item === currentElement.table) {
                    return;
                }
                let find = groupResult.find(any => any.table === item)
                if (!find) {
                    find = group.find(any => any.table === item)
                    if (find) {
                        let child = { ...find, level: currentElement.level + 1 }
                        // console.log(3, child)
                        groupResult.push(child)
                    } else {
                        console.log(`cannot find child ${item}`)
                    }
                } else {
                    loop.push(item)
                    levelPlus(item, currentElement.level)
                    loop.splice(0)
                }
            })
        }
    });
    function levelSorter(a, b) {
        return a.level - b.level
    }

    function levelPlus(table, min) {
        let find = groupResult.find(any => any.table === table)
        if (!find) {
            return;
        }
        if (find.level <= min) {
            find.level = find.level + 1;
        }
        if (find.child_tables && find.child_tables.length > 0) {
            find.child_tables.forEach(item => {
                if (!loop.includes(item)) {
                    loop.push(item)
                    levelPlus(item, find.level)
                }
            })
        }
    }

    return groupResult.sort(levelSorter)
}
