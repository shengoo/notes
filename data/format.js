const path = require('path')
const fs = require('fs')

console.time('Total time')
const data = require('./xueyuan.json')
console.log(data.length)
const result = [];
const loop = []
data.forEach(element => {
    let current = result.find(any => any.value === element.table) || {};
    if (!current.value) {
        current.value = element.table;
        current.level = 1;
        result.push(current)
    }
    if (element.parent_tables
        && element.parent_tables.filter(item => item !== '').length > 0) {
        let parents = element.parent_tables.filter(item => item !== '')
        current.parent_tables = parents
    }
    if (element.child_tables
        && element.child_tables.filter(item => item !== '').length > 0) {
        let children = element.child_tables.filter(item => item !== '')
        current.child_tables = children
    }
    if (current.parent_tables) {
        current.level = 2;
        // 遍历上游
        current.parent_tables.forEach(item => {
            // 上游元素不在结果中
            let find = result.find(any => any.value === item)
            if (!find) {
                // 插入上游元素
                let parent = { value: item, level: 1 }
                result.push(parent)
            } else {
                if (find.level + 1 < current.level)
                    current.level = find.level + 1;
            }
        })
    }
    if (current.child_tables) {
        current.child_tables.forEach(item => {
            let find = result.find(any => any.value === item)
            if (!find) {
                let child = { value: item, level: 2 }
                result.push(child)
            } else {
                loop.push(item)
                levelPlus(item, current.level)
                loop.splice(0)
            }
        })
    }
});

console.log(result.sort(sorter).slice(0, 3))
const filename = path.resolve(__dirname, `result${new Date().valueOf()}.json`)
fs.writeFileSync(filename, JSON.stringify(result, null, 4), 'utf8');
console.timeEnd('Total time')

function sorter(a, b) {
    return a.level - b.level
}

function levelPlus(table, min) {
    let find = result.find(any => any.value === table)
    if (!find) {
        return;
    }
    if (find.level <= min) {
        find.level = find.level + 1;
    }
    if (find.child_tables && find.child_tables.length > 0) {
        find.child_tables.forEach(item => {
            if (!loop.includes(item)) {
                loop.push(item)
                levelPlus(item, find.level)
            }
        })
    }
}
