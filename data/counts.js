const path = require('path')
const fs = require('fs')

console.time('Total time')
const data = require('./xueyuan2.json')

console.log(data.length) // 24121

// 单点
const singles = data.filter(item => !item.parent_tables && !item.child_tables)
console.log(singles.length) // 8970

// 只有下游没有上游的根结点
const roots = data.filter(item => !item.parent_tables && item.child_tables)
console.log(roots.length) // 6925

// 只有上游没有下游的叶子节点
const leafs = data.filter(item => item.parent_tables && !item.child_tables)
console.log(leafs.length) // 4897

// 有上游且有下游的节点
const middles = data.filter(item => item.parent_tables && item.child_tables)
console.log(middles.length) // 3329

// 除了单点之外的其他
const relates = data.filter(item => item.parent_tables || item.child_tables)
console.log(relates.length) // 15151

console.timeEnd('Total time')
