# Object.defineProperty()

> es5, no shim, IE9+

The Object.defineProperty() method defines a new property directly on an object, or modifies an existing property on an object, and returns the object.

## Syntax
Object.defineProperty(obj, prop, descriptor)

### Parameters
obj

    The object on which to define the property.

prop

    The name of the property to be defined or modified.

descriptor

    The descriptor for the property being defined or modified.

### Return value
The object that was passed to the function.