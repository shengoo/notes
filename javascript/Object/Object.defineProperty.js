var obj = {};
Object.defineProperty(obj, 'key', {
    enumerable:true,
    get: function () {
        return this._key;
    },
    set: function (newValue) {
        console.log(this);
        //   Reflect.set(obj, 'key', newValue);
        this._key = newValue
    }
});
Object.defineProperty(obj, '_key', {
    enumerable:false,
});

obj.key = 1;
console.log(obj);
