function extend(destination, source) {
    for(var key in source) {
        if(!destination[key]) {
            destination[key] = source[key];
        }
    }
    return destination;
}

function Base() {
    this.name = 'name';
    this.baseFun = function() {
        console.log('function in Base');
    }
}

function Child() {

}

var base = new Base();
var child = new Child();

extend(child, base);

child.baseFun();
console.log(child.name)

// var a = new Child();
// a.baseFun();
