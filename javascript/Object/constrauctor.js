var $ = function (argument) {
	$.arglength = argument.length;
	return $;
}

$.hello = function (argument) {
	console.log($.arglength)
	return $;
}

var a = $([1,2]);
var b = $([1,2,4]);

console.log(a === b);
