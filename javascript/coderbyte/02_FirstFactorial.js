function FirstFactorial(num) {
  // code goes here
  return num > 1 ? num * FirstFactorial(num - 1) : 1;

}

// keep this function call here
// to see how to enter arguments in JavaScript scroll down
console.log(FirstFactorial(8));
