// Challenge
// Using the JavaScript language, have the function LetterCapitalize(str) take the str parameter being passed and capitalize the first letter of each word. Words will be separated by only one space.
// Sample Test Cases
// Input:"hello world"

// Output:"Hello World"


// Input:"i ran there"

// Output:"I Ran There"

function LetterCapitalize(str) {

    var arr = str.split(' ');
    for(var i = 0; i < arr.length; i++) {
        arr[i] = Capitalize(arr[i]);
    }
    // arr.forEach((item, index) => {
    //     arr[index] = Capitalize(arr[index]);
    // });

    function Capitalize(word) {
        var arr = word.split('');
        arr[0] = arr[0].toUpperCase();
        return arr.join('');
    }

  // code goes here
  return arr.join(' ');

}

// keep this function call here
console.log(LetterCapitalize("hello world"));
console.log(LetterCapitalize("i ran there"));
