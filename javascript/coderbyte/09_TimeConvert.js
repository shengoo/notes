/**

Challenge
Using the JavaScript language,
have the function TimeConvert(num) take the num parameter being passed
and return the number of hours and minutes the parameter converts to
(ie. if num = 63 then the output should be 1:3).

Separate the number of hours and minutes with a colon.


Sample Test Cases
Input:126

Output:"2:6"


Input:45

Output:"0:45"

**/


function TimeConvert(num) {

    var hours = Math.floor(num / 3600);
    var minutes = Math.floor((num - 3600 * hours) / 60);
    var seconds = num % 60;
    return hours > 0 ? hours + ':' + minutes + ':' + seconds : minutes + ':' + seconds;

}

// keep this function call here
console.assert(TimeConvert(126) === "2:6");
console.assert(TimeConvert(45) === "0:45");
