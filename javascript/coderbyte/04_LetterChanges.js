// Have the function LetterChanges(str) take the str parameter being passed and modify it using the following algorithm.
// Replace every letter in the string with the letter following it in the alphabet (ie. c becomes d, z becomes a).
// Then capitalize every vowel in this new string (a, e, i, o, u) and finally return this modified string.

function LetterChanges(str) {


  var newStr = "";

  for (var i = 0; i < str.length; i++) {
    newStr += GetNewLetter(str[i]);
  };
  return newStr;

}

function GetNewLetter (letter) {
    if(letter.charCodeAt(0)<97 || letter.charCodeAt(0)>122)
        return letter;
    var code = letter.charCodeAt(0) + 1;
    var newL = String.fromCharCode(code);
    if(newL === "a" || newL === "e" || newL === "i" || newL === "o" || newL === "u")
        newL = newL.toUpperCase();
    //console.log(newL)
    return newL;
}

// keep this function call here
// to see how to enter arguments in JavaScript scroll down
console.log(LetterChanges("Argument goes here")); // AshvnfOU hpft Ifsf
