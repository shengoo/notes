/**
Challenge
Using the JavaScript language, have the function SimpleSymbols(str) take the str parameter being passed
and determine if it is an acceptable sequence by either returning the string true or false.
The str parameter will be composed of + and = symbols with several letters between them (ie. ++d+===+c++==a)
and for the string to be true each letter must be surrounded by a + symbol.
So the string to the left would be false. The string will not be empty and will have at least one letter.


Sample Test Cases
Input:"+d+=3=+s+"

Output:"true"


Input:"f++d+"

Output:"false"
**/

const assert = require('assert');

function SimpleSymbols(str) {

  // code goes here
  return str.split('+=').length > 1 || str.split('=+').length > 1;

}

// keep this function call here
console.assert(SimpleSymbols("+d+=3=+s+") === true);
console.assert(SimpleSymbols("f++d+") === false);

console.assert(SimpleSymbols("+d+") === true); // true
console.assert(SimpleSymbols("+d===+a+") === false); // false
console.assert(SimpleSymbols("+z+z+z+") === true); // true
console.assert(SimpleSymbols("2+a+a+") === true); // true
console.assert(SimpleSymbols("+a++") === true); // true


