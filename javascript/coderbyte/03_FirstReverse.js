function FirstReverse(str) {

  // code goes here
  return str.split('').reverse().join('');

}

// keep this function call here
// to see how to enter arguments in JavaScript scroll down
console.log(FirstReverse("Argument goes here"));
