function LongestWord(sen) {

  var str = "";
  var tmp = "";
  for(var i = 0,maxlength = sen.length;i<maxlength;i++){
    //console.log(sen[i]);
    if(isChar(sen[i])){
      tmp+=sen[i];
    }else{
      console.log(str + " " + tmp);
      if(str.length < tmp.length){
        str = tmp;
        tmp = "";
      }else{
        tmp = "";
      }
    }
  }
  if(str.length < tmp.length){
    str = tmp;
    tmp = "";
  }
  // code goes here

  console.log('result: ' + str)
  return str;

}

function isChar(chr){
  return (chr.charCodeAt(0) >= 65 && chr.charCodeAt(0) <= 91)
      || (chr.charCodeAt(0) >= 97 && chr.charCodeAt(0) <= 122);
}

// keep this function call here
// to see how to enter arguments in JavaScript scroll down
LongestWord("Argument goes here");
