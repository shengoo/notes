
```js
var result = (function() {
    var small = {};
    var big = new Array(10000000);
    //do something
    return function(){
        if(big.indexOf("someValue") !== -1) {
            return null;
        } else {
            return small;
        }
    }
})();
```

这里，创建了一个闭包。使得返回的函数存储在result中，而result函数能够访问其作用域内的small对象和big对象。由于big对象和small对象都可能被访问，所以垃圾回收器不会去碰这两个对象，它们不会被回收。我们将上述代码改成如下形式：

```js
var result = (function() {
    var small = {};
    var big = new Array(10000000);
    var hasSomeValue;
    //do something
    hasSomeValue = big.indexOf("someValue") !== -1;
    return function(){
        if(hasSomeValue) {
            return null;
        } else {
            return small;
        }
    }
})();
```

这样，函数内部只能够访问到hasSomeValue变量和small变量了，big没有办法通过任何形式被访问到，垃圾回收器将会对其进行回收，节省了大量的内存。