var person1 = new Object({
  name: 'Chris',
  age: 38,
  greeting: function() {
    console.log('Hi! I\'m ' + this.name + '.');
  }
});

console.log(person1)

var person2 = Object.create(person1);
console.log(person2) // {}
console.log(person2.__proto__) // person1
for(var key in person2) {
    console.log(key)
}
