```js
function copyToClp(x) {
	var d = document, b = d.body, g = window.getSelection;
	x = d.createTextNode(x);
	b.appendChild(x);
	if (b.createTextRange) {
		var t = b.createTextRange();
		t.moveToElementText(x);
		t.select();
		d.execCommand('copy');
	} else {
		var t = d.createRange();
		t.selectNodeContents(x);
		g().removeAllRanges();
		g().addRange(t);
		d.execCommand('copy');
		g().removeAllRanges();
	}
	x.remove();
}
```