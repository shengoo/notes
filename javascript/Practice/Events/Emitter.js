// on, off, once, trigger

function Emitter(){
    this.handlers = {};
}

Emitter.prototype = {
    on: function(type, handler) {
        if(this.handlers[type]) {
            this.handlers[type].push(handler);
        } else {
            this.handlers[type] = [handler];
        }
    },
    off: function(type, handler) {
        if(this.handlers[type]){
            var index = this.handlers[type].indexOf(handler);
            if(index > -1) {
                this.handlers[type].splice(index,1);
            }
        }
    },
    once: function(type, handler) {
        handler.tag = 'once';
        this.on(type, handler);
    },
    trigger: function(type) {
        if(this.handlers[type]){
            this.handlers[type].forEach(handler => {
                handler();
            });
            this.handlers[type] = this.handlers[type].filter(function(item) {
                return item.tag != 'once';
            });
        }
    },
}

var e = new Emitter();
function c1() {
    console.log('c1');
}
function c2() {
    console.log('c2');
}
e.once('c', c1);
e.on('c', c2);
e.on('c', function() {
    console.log('c3')
});
e.trigger('c'); // c1, c2, c3
e.trigger('c'); // c2, c3
e.off('c', c2);
e.trigger('c'); // c3
