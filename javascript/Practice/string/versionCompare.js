function isNewerVersion (oldVer, newVer) {
  const oldParts = oldVer.split('.')
  const newParts = newVer.split('.')
  for (var i = 0; i < newParts.length; i++) {
    const a = parseInt(newParts[i]) || 0
    const b = parseInt(oldParts[i]) || 0
    // console.log(a,b)
    if (a > b) return -1
    if (a < b) return 1
  }
  return 0;
}

console.log(isNewerVersion('1.0', '2.0')) // -1
console.log(isNewerVersion('1.0', '1.0.1')) // -1
console.log(isNewerVersion('1.0.1', '1.0.10')) // -1
console.log(isNewerVersion('1.0.1', '1.0.1')) // 0
console.log(isNewerVersion('2.0', '1.0')) // 1
console.log(isNewerVersion('2', '1.0')) // 1
console.log(isNewerVersion('2.0.0.0.0.1', '2.1')) // -1
console.log(isNewerVersion('2.0.0.0.0.1', '2.0')) // 1

var versions = [
  '1.5.19',
  '1.2.3.0',
  '1.2.3',
  '1.2.3.0',
  '1.5.5'
]
var sorted = versions.sort(isNewerVersion);
console.log(sorted)
