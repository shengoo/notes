
/**
 * 2. fastEqual
 *
 * 示例输入: a = { arr: [1, 2], num: 12 } b = { arr: [1, 2], num: 12 }
 *
 * 示例输出: true
 *
 * tips: 不需要考虑太多边界情况, 保证常见数据结构, 优先效率.
 */

const a = { arr: [1, 2], num: 12, obj: {} };
const b = null;

function fastEqual(a, b) {
    // 基础类型
    if(a === b) {
        return true;
    }
    // 数组
    if(Array.isArray(a) && Array.isArray(b)){
        if(a.length !== b.length){
            return false;
        }
        for(var i = 0, length = a.length; i < length; i++) {
            if(!fastEqual(a[i], b[i])){
                return false;
            }
        }
        return true;
    }
    // 对象
    for(var key in a){
        if(!b[key]){
            return false;
        }
        if(!fastEqual(a[key], b[key])){
            return false;
        }
    }
    return true;
}

console.log(fastEqual(a, b));
