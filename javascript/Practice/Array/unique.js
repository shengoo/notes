const inputArray = [1, 3, 1, 2, 3];
function unique(input) {
    return input.reduce(function(accu,curr){
        if(accu.indexOf(curr) === -1) {
            accu.push(curr);
        }
        return accu;
    }, []);
    // return Object.keys(input.reduce(function(accu,curr){
    //     accu[curr] = 1;
    //     return accu;
    // }, {}));
}
console.log(unique(inputArray))
