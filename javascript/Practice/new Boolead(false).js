var a = new Boolean(false);
console.log(a); // [Boolean: false]
console.log(a.valueOf()); // false
console.log(a == false); // true
console.log(a === false); // false
if(a){
	console.log("new Boolean(false) is true")
}else{
	console.log("new Boolean(false) is false")
}
