

function Pro(fn) {
    this._state = 0;
    fn(function(value){
        console.log('resolve')

    }, function(reason){
        console.log('reject')

    });
}

Pro.prototype = {
    then: function(onFulfilled, onRejected) {

    },
    'catch': function(onRejected) {
        this.then(null, onRejected);
    },
}

var p = new Pro(function(resolve, reject){
    var date = (new Date()).valueOf();
    if(date % 2){
        resolve('1');
    }else {
        reject('2')
    }
});
p.then(function(value) {
    console.log('resolve' + value);
},function(reason){
    console.log('reject' + reason);
})

