# JavaScript HTML DOM Document

The HTML DOM document object is the owner of all other objects in your web page.

## Finding HTML Elements

```js
document.getElementById(id)
document.getElementsByTagName(name)
document.getElementsByClassName(name)
```

## Changing HTML Elements

```js
element.innerHTML =  'new html content'	
element.attribute = 'new value'
element.setAttribute(attribute, 'value')
element.style.property = 'new style'
```

## Adding and Deleting Elements

```js
document.createElement(element)
document.removeChild(element)
document.appendChild(element)
document.replaceChild(element)
document.write(text)
```

## Adding Events Handlers

```js
document.getElementById(id).onclick = function(){code}
```
