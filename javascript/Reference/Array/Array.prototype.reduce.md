# reduce

`reduce()` 方法对累计器和数组中的每个元素（从左到右）应用一个函数，将其简化为单个值。

```js
const array1 = [1, 2, 3, 4];
const reducer = (accumulator, currentValue) => accumulator + currentValue;

// 1 + 2 + 3 + 4
console.log(array1.reduce(reducer));
// expected output: 10

// 5 + 1 + 2 + 3 + 4
console.log(array1.reduce(reducer, 5));
// expected output: 15
```

reducer 函数接收4个参数:

1. Accumulator (acc) (累计器)
1. Current Value (cur) (当前值)
1. Current Index (idx) (当前索引)
1. Source Array (src) (源数组)

您的 reducer 函数的返回值分配给累计器，其值在数组的每个迭代中被记住，并最后成为最终的单个结果值。

回调函数第一次执行时，accumulator 和currentValue的取值有两种情况：

1. 如果调用reduce()时提供了initialValue，accumulator取值为initialValue，currentValue取数组中的第一个值；

1. 如果没有提供 initialValue，那么accumulator取数组中的第一个值，currentValue取数组中的第二个值。
