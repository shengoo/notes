```js
export Some from './Some';
export {default as SomeClass} from './SomeClass';
export {someFunction} from './utils';
export {default as React} from 'react';
```
