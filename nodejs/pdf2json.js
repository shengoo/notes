let fs = require('fs'),
    PDFParser = require("pdf2json"),
    path = require('path');

let pdfParser = new PDFParser(this,1);

pdfParser.on("pdfParser_dataError", errData => {
    console.error(errData)
});
pdfParser.on("pdfParser_dataReady", pdfData => {
    // console.log(pdfData)
    // fs.writeFile("./pdf2json.json", JSON.stringify(pdfData));
    const filename = path.resolve(__dirname, 'pdf2json.txt')
    // fs.writeFileSync(filename, JSON.stringify(pdfData, null, 4));
    fs.writeFileSync(filename, pdfParser.getRawTextContent())
});

pdfParser.loadPDF("/Users/shengqing/code/github/notes/nodejs/1540524854107.pdf");
