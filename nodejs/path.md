```js
var path = require('path');

console.log(__dirname);
console.log(__filename);
console.log(process.cwd());
console.log(path.resolve('./'));
```

* __dirname: 总是返回被执行的 js 所在文件夹的绝对路径
* __filename: 总是返回被执行的 js 的绝对路径
* process.cwd(): 总是返回运行 node 命令时所在的文件夹的绝对路径
* ./: 跟 process.cwd() 一样,在require的时候才是相对路径。
