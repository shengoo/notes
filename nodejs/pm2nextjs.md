```
# for development
pm2 start npm --name "next" -- run dev

# for production
npm run build
pm2 start npm --name "next" -- start
```

```
"scripts": {
    "pm2-next": "next build && pm2 start ./node_modules/next/dist/bin/next-start",
    "pm2-next-cluster": "next build && pm2 start ./node_modules/next/dist/bin/next-start -i max",
}
```