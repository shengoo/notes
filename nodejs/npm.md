# NPM
Node Package Manager
https://docs.npmjs.com/

## Install
Comes with Nodejs installer
### Update npm
```npm install npm@latest -g```

## Installing npm packages locally
```npm install <package_name>```
### example
```npm install jet-api```

### Using the installed package

```js
var lodash = require('lodash');
//using babel
import lodash from 'lodash';
```

## package.json

1. It serves as documentation for what packages your project depends on.
1. It allows you to specify the versions of a package that your project can use using semantic versioning rules.
1. It makes your build reproducible, which means that it's much easier to share with other developers.

### generate a package.json file
```npm init -y```

* "dependencies": These packages are required by your application in production.
* "devDependencies": These packages are only needed for development and testing.


## install

### from git
`npm i -S git+ssh://git@git.sami.int.thomsonreuters.com:jet/jet-plugin-news.git#2.1.2`

### Publishing a Public Scoped Package

`npm publish --access public`

## deprecate

`npm deprecate my-thing@"< 0.2.3" "critical bug fixed in v0.2.3"`
