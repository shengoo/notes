# fs常用操作

## existsSync

```js
fs.existsSync(filename)
```

## isDirectory

```js
fs.lstatSync(containerDir).isDirectory()
```

## mkdir

```js
fs.mkdirSync(path[, options])
```
