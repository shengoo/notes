# Use `n` to manage your Node.js versions

## Installation

1. Install n through `npm`:(Recommended)

```bash
npm install -g n
```

## Configurations

Since `n`'s default path is `/usr/local/n`, it will need super user's permission to modify file systems in the default path, we need to config `n`'s path to user's path.

### config `N_PREFIX`

1. edit bash profile

    ```bash
    vim ~/.bashrc
    ```

1. add line

    ```bash
    export N_PREFIX=~/.n
    ```

1. apply file

    ```bash
    source ~/.bashrc
    ```

### config PATH

1. edit bash profile

    ```bash
    vim ~/.bashrc
    ```

1. add a line

    ```bash
    export PATH=$HOME/.n/bin:$PATH
    ```

1. apply file

    ```bash
    source ~/.bashrc
    ```

## Use

```bash
# Use or install a version of node
n 9.0.0

# Use or install the latest official release
n latest

# Use or install the stable official release
n stable

# Use or install the latest LTS official release
n lts
```

Type `n` to show list of versions.

And select a version by <kbd>up</kbd> <kbd>down</kbd> button.

```bash
$ n

    node/8.9.3
    node/9.0.0
    node/9.2.1
  ο node/9.3.0

```