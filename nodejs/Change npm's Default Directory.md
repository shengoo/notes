# How to Prevent Permissions Errors

Back-up your computer before you start.

1. Make a directory for global installations:

```shell
mkdir ~/.npm-global
```

1. Configure npm to use the new directory path:

```shell
npm config set prefix '~/.npm-global'
```

1. Open or create a ~/.profile file and add this line:

```shell
export PATH=~/.npm-global/bin:$PATH
```

1. Back on the command line, update your system variables:

```shell
source ~/.profile
# or
source ~/.bash_profile
```
