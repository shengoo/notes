# install tomcat

## mac

### install

```
brew install tomcat@8
```

### start service

```
brew services start tomcat@8
```

## Tomcat 8 centos 6

1. install

```
wget http://ftp.riken.jp/net/apache/tomcat/tomcat-8/v8.0.20/bin/apache-tomcat-8.0.20.tar.gz 
tar zxvf apache-tomcat-8.0.20.tar.gz 
mv apache-tomcat-8.0.20 /usr/tomcat8 
useradd -M -d /usr/tomcat8 tomcat8 
chown -R tomcat8. /usr/tomcat8
```

2. create init script ```vi /etc/rc.d/init.d/tomcat8```

```shell
 #!/bin/bash

# Tomcat8: Start/Stop Tomcat 8
#
# chkconfig: - 90 10
# description: Tomcat is a Java application Server.

. /etc/init.d/functions
. /etc/sysconfig/network

CATALINA_HOME=/usr/tomcat8
TOMCAT_USER=tomcat8

LOCKFILE=/var/lock/subsys/tomcat8

RETVAL=0
start(){
    echo "Starting Tomcat8: "
    su - $TOMCAT_USER -c "$CATALINA_HOME/bin/startup.sh"
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && touch $LOCKFILE
    return $RETVAL
}

stop(){
    echo "Shutting down Tomcat8: "
    $CATALINA_HOME/bin/shutdown.sh
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && rm -f $LOCKFILE
    return $RETVAL
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        stop
        start
        ;;
    *)
        echo $"Usage: $0 {start|stop|restart}"
        exit 1
        ;;
esac
exit $?
```

3. permission

```
chmod 755 /etc/rc.d/init.d/tomcat8 
```

4. start

```
/etc/rc.d/init.d/tomcat8 start
```

5. config

```
chkconfig --add tomcat8 
chkconfig tomcat8 on
```

## config

### manager remote access

vim {tomcat}/webapps/manager/META-INF/context.xml

```xml
<Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="^.*$" />
```
