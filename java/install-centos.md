# install java on centos

## centos6

### OpenJDK 8

#### Install OpenJDK 8 JRE

```
sudo yum install java-1.8.0-openjdk
```

#### Install OpenJDK 8 JDK

To install OpenJDK 8 JDK using yum, run this command:

```
sudo yum install java-1.8.0-openjdk-devel
```
