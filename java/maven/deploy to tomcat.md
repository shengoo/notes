# Deploy war file to tomcat using maven

1. Tomcat config on server

in `$CATALINA_BASE/conf/tomcat-users.xml`

```xml
<user username="username" password="secret" roles="admin,manager-script" />
```

1. Config server credentials in you ~/.m2/settings.xml file

```xml
<settings>
  <servers>
    <server>
      <id>tomcatserver</id>
      <username>admin</username>
      <password>admin</password>
    </server>
  </servers>
</settings>
```

2、Add plugin in your pom.xml

```xml
<plugin>
  <groupId>org.apache.tomcat.maven</groupId>
  <artifactId>tomcat7-maven-plugin</artifactId>
  <version>2.2</version>
  <configuration>
    <url>http://localhost:8080/manager/text</url>
    <server>tomcatserver</server>
    <path>/sss</path>
    <username>mavenuser</username>
    <password>pass@word1</password>
  </configuration>
</plugin>
```

3、Run command

```shell
mvn clean tomcat7:deploy
```
