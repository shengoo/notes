# Mac开发配置

## System Preferences

 > 关于本机 > 软件更新

### 触控板

*  > 系统偏好设置 > 触控板
	* 光标与点击
		* ✓ 查询与数据监测器 > 用一个手指用力点按
		* ✓ 辅助点按 > 用两个手指点按或轻点
		* ✓ 轻拍来点按
		* ✓ 静默点按（声音变小）
		* ✓ 用力点按和触觉反馈
	* 滚动缩放
		* ✓ 默认全选
	* 更多手势
		* ✓ 默认全选
*  > 系统偏好设置 > 辅助功能

	启用三指拖移
	![](assets/move.png?raw=true)

*  > 系统偏好设置 > 键盘 > 快捷键

	打开所有控制
	![](assets/allcontrols.png)


### Dock

*  > 系统偏好设置 > Dock
	* ✓ 将窗口最最小化为应用程序图标
	* ✓ 自动显示和隐藏 Dock

### Finder
* Finder > 显示
	* 显示标签页栏
	* 显示路径栏
	* 显示状态栏

* Finder > 偏好设置
	* 通用
		* 开启新 Finder 窗口时打开：HOME「用户名」目录
	* 边栏
		* 添加 HOME「用户名」目录 和 创建代码文件目录
		* 将 共享的(shared) 和 标记(tags) 目录去掉	

### Show/Hide Hidden Files the Long Way

1. Open Terminal found in Finder > Applications > Utilities
1. In Terminal, paste the following: `defaults write com.apple.finder AppleShowAllFiles YES`
1. Press return
1. Hold the ‘Option/alt’ key, then right click on the Finder icon in the dock and click Relaunch.

### quicklook plugins

quicklook就是在finder中选中文件之后，点击空格出现的文件预览

安装插件可以使quicklook支持更多文件类型

```js
// run after brew cask installed
brew cask install qlcolorcode
brew cask install qlstephen
brew cask install qlmarkdown
brew cask install quicklook-json
brew cask install qlprettypatch
brew cask install quicklook-csv
brew cask install betterzipql
brew cask install webpquicklook
brew cask install suspicious-package
brew cask install stringsfile
brew cask install quicklookapk
```

### Scroll Reverser

当你在浏览一个很长的网页时，你看完了当前显示的内容，想要看后续的内容，你可以在 Trackpad 上双指上滑，或者鼠标滚轮向上滚动。这是被称作“自然”的滚动方向。

然而在 Windows 里鼠标滚动的行为是相反的：鼠标滚轮向下滚动才会让浏览器显示后续的内容，向上滚动会达到页面的顶部。你可以在 OS X 的系统偏好设置里修改（选择System Preferences >Trackpad，在Scroll & Zoom标签页中不选中Scroll direction: natural），但是这样会同时改变鼠标滚轮的方向和 Trackpad 的方向。

要想只改变鼠标滚轮的方向，而保持 Trackpad 依旧是“自然”的，我们需要 Scroll Reverser：

`brew cask install scroll-reverser`

### vim配置

`vi ~/.vimrc`

```
color desert "颜色设置
syntax on "语法高亮
set number "自动显示行号
set cursorline "突出显示当前行
set ts=4 "设置tab长度为4
set shiftwidth=4 "设定 << 和 >> 命令移动时的宽度为 4
```

## Homebrew

* Homebrew : package manager for macOS

	包管理工具，官方称之为The missing package manager for OS X。

	安装步骤：先打开 Terminal 应用，输入：

	`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

	有了 brew 以后，要下载工具，比如 MySQL、Gradle、Maven、Node.js 等工具，就不需要去网上下载了，只要一行命令就能搞定：

	`brew install mysql gradle maven node`

* Homebrew-Cask

	brew-cask 允许你使用命令行安装 OS X 应用。比如你可以这样安装 Chrome：brew cask install google-chrome。还有 Evernote、Skype、Sublime Text、VirtualBox 等都可以用 brew-cask 安装。

	安装：
	
	`brew tap caskroom/cask`

## iterm2

> iTerm2 是最常用的终端应用，是 Terminal 应用的替代品。提供了诸如Split Panes等一群实用特性。

安装：

`brew cask install iterm2`

## Git

XCode会安装git，无需单独安装。

* SourceTree

	SourceTree 是 Atlassian 公司出品的一款优秀的 Git 图形化客户端。如果你发现命令行无法满足你的要求，可以试试 SourceTree。

	安装：

	`brew cask install sourcetree`
	用 brew-cask 安装会自动增加命令行工具stree到$PATH里。在命令行中输入stree可以快速用 SourceTree 打开当前 Git 仓库。详细用法请参见stree --help。

## Text Editors

* Sublime Text
	
	`brew cask install sublime-text`

	* 安装Package Control

	`import urllib.request,os,hashlib; h = '6f4c264a24d933ce70df5dedcf1dcaee' + 'ebe013ee18cced0ef93d5f746d80ef60'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)`

* Visual Studio Code

	`brew cask install visual-studio-code`

##  Softwares

* Browsers

	* Chrome

		`brew cask install google-chrome`
		
1. Alfred:搜索工具

	```
    brew cask install alfred
    ```

1. dropbox:网盘

    ```
    brew cask install dropbox
    ```

1. postman:网络请求
	```
    brew cask install postman
    ```

1. teamviewer:远程桌面

	```
    brew cask install teamviewer
    ```

1. shadowsocksx-ng:翻墙工具

	```
    brew cask install shadowsocksx-ng
    ```

1. BetterSnapTool:拖放窗口工具

	https://itunes.apple.com/cn/app/bettersnaptool/id417375580?mt=12

1. Go2Shell:在目录里打开terminal

	https://itunes.apple.com/cn/app/go2shell/id445770608?mt=12

	```
    brew cask install go2shell
    ```

1. Sip:取色工具

	```
    brew cask install sip
    ```

1. Snip:截图工具

	https://itunes.apple.com/cn/app/snip/id512505421?mt=12

	```
    brew cask install snip
    ```

1. The Unarchiver:解压缩

	https://itunes.apple.com/cn/app/the-unarchiver/id425424353?mt=12

	```
    brew cask install the-unarchiver
    ```

1. ImageOptim:压缩图片

	```
    brew cask install imageoptim
    ```

## NodeJS

`brew install nodejs`

NodeJS Packages

* n : Node version management

	`npm install -g n`
* change nodejs to LTS version

	```
	mkdir /usr/local/n
	sudo mkdir /usr/local/n
	sudo chown -R $(whoami) /usr/local/n
	n lts
	```

## Java

* install java 8 instead of java 9

	```
	brew tap caskroom/versions
	brew cask install java8
	```

* Maven

	`brew install maven`

## IDE

* XCode

	https://itunes.apple.com/cn/app/xcode/id497799835?mt=12

	* CocoaPods

		`sudo gem install cocoapods`

* Android Studio

	`brew cask install android-studio`

* IntelliJ Idea

	`brew cask install intellij-idea`

* WebStorm

	`brew cask install webstorm`


