brew services list

[sudo] brew services run formula|--all
Run the service formula without starting at login (or boot).

[sudo] brew services start formula|--all
Start the service formula immediately and register it to launch at login (or boot).
