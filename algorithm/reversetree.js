var tree = {
    value: 'A',
    left: {
        value: 'B',
        left: {
            value: 'D',
            left: null,
            right: null,
        },
        right: {
            value: 'E',
        left: null,
        right: null
      }
    },
    right: {
      value: 'C',
      left: {
        value: 'F',
        left: null,
        right: null
      },
      right: {
        value: 'G',
        left: null,
        right: null
      }
    }
  }



  function reverse(tree){
    if(!tree){
      return null;
    }
    if(tree.left && tree.right){
      var temp = tree.left;
      tree.left = tree.right;
      tree.right = temp;
    }
    tree.left = reverse(tree.left);
    tree.right = reverse(tree.right);
    return tree;
  }

  console.log(reverse(tree))
