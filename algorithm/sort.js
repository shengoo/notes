var arr = [4, 2, 5, 8, 6, 9, 7, 10, 1, 3];

function swap(array, index1, index2) {
    if(index1 === index2)
        return;
    console.log(array, index1, index2);
    array[index1] = array[index1] + array[index2];
    array[index2] = array[index1] - array[index2];
    array[index1] = array[index1] - array[index2];
}

function move(array, index1, index2) {
    var a = array.splice(index1, 1);
    array.splice(index2, 0, a[0]);
}

// 冒泡排序(Bubble Sort)
function bubleSort(array) {
    for(var j = array.length - 1; j > 0; j--) {
        for(var i = 0; i < j; i++) {
            if(array[i] > array[i + 1]) {
                swap(array, i, i + 1);
            }
        }
    }
}

// 选择排序(Selection Sort)
function selectSort(array) {
    for(var i = 0; i < array.length; i++){
        var minindex = i;
        for(var j = i; j < array.length; j++){
            if(array[j] < array[minindex]){
                minindex = j;
            }
        }
        if(minindex !== i){
            swap(array, minindex, i);
        }
    }
}

// 插入排序(Insertion Sort)
function insertSort(array) {
    for(var i = 1; i < array.length; i++) {
        for(var j = 0; j < i; j++){
            if(array[j] > array[i]){
                console.log(`move ${i} to ${j}`)
                move(array, i, j);
            }
        }
    }
}

// 快速排序(Quick Sort)
function quickSort(array, left, right) {
    // console.log(left, right);
    if(left >= right){
        return;
    }
    var pivotIndex = partition(array, left, right);
    quickSort(array, left, pivotIndex - 1);
    quickSort(array, pivotIndex + 1, right);
}

function partition(array, left, right) {
    // 找个基准
    var pivot = array[right];
    var tail = left - 1;
    for(var i = left; i < right; i++) {
        if(array[i] < pivot){
            // console.log(array[i] , pivot)
            tail = tail + 1;
            swap(array, i, tail);
        }
    }
    swap(array, tail + 1, right);
    return tail + 1;
}



// bubleSort(arr);
// selectSort(arr);
// insertSort(arr);
quickSort(arr, 0, arr.length - 1);
console.log(arr);
