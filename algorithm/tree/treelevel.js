var tree = {
    value: 1,
    left: {
        value: 2,
        left: {
            value: 4,
            left: null,
            right: null,
        },
        right: {
            value: 5,
            left: null,
            right: null,
        }
    },
    right: {
        value: 3,
        left: {
            value: 6,
            left: null,
            right: null,
        },
        right: {
            value: 7,
            left: null,
            right: null,
        }
    }
};

var result = {};
function gettree(tree, level){
    if(result[level]){
        result[level].push(tree.value)
    }else{
        result[level] = [tree.value];
    }
    console.log(result)
    if(tree.left && tree.right){
        level++;
        gettree(tree.left, level);
        gettree(tree.right, level);
    }
}
gettree(tree,1);
console.log(result)
