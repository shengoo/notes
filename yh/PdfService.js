const request = require('request');
const jsdom = require("jsdom");
const fs = require('fs');
const path = require('path');
const PDFParser = require("pdf2json");
const MongoClient = require('mongodb').MongoClient;
const dburl = 'mongodb://localhost:27017';
const {JSDOM} = jsdom;
const {window} = new JSDOM();
$ = require('jquery')(window);

const statusCollectionName = 'status';
const resultsCollectionName = 'results';

class PdfService {
	refreshData() {
		this.refresh();
	}


	async refresh() {
		const results = await this.getResults();
		for (const result of results) {
			const detail = await this.getResultDetail(result);
			for (const pdf of detail) {
				await this.fetchPdfData(pdf);
			}
		}
		// console.log(`refresh complete`)
	}

	async getResults() {
		return new Promise((resolve, reject) => {
			request(this.baseUrl, (error, response, body) => {
				if (error) {
					console.error(error)
					reject(error)
					return;
				}
				const results = [];
				$('a:contains(（摇号）配置结果)', body).each(async (index, element) => {
					const link = this.formatLink(element.href);
					const text = element.innerHTML;
					const match = text.match(/\d+/g);
					if (match.length === 2) {
						results.push({
							month: match[0] + match[1],
							link,
						})
					}
				});
				resolve(results);
			});
		});
	}

	/**
	 *
	 * @param {month, link} resultInfo
	 *
	 */
	async getResultDetail(resultInfo) {
		return new Promise((resolve, reject) => {
			request(resultInfo.link, (error, response, body) => {
				if (error) {
					console.error(error)
					reject(error)
				}
				const results = [];
				$('a[href$=".pdf"]', body).each((index, element) => {
					const link = this.formatLink(element.href);
					const text = element.innerHTML;
					const type = this.getType(text);
					results.push({
						type,
						link,
						month: resultInfo.month
					})
				});
				resolve(results);
			})
		});
	}

	async fetchPdfData(info) {
		const dataReady = await this.checkInfo(info);
		if (dataReady) {
			console.log(`${info.month} ${info.type} ready`)
			return;
		}
		console.log(`${info.month} ${info.type} gets`)
		const file = await this.downloadPdf(info);
		const content = await this.getPdfContent(file);
		// const content = await getPdfContent('cache/city201809-个人节能车-1542554064336.pdf');
		// const content = await getPdfContent('yh/bj.pdf');
		// console.log(content.split('\n').length)
		const data = this.getDataFromContent(content, info.type, info.month);
		const count = await this.insertData(data.issues)
		if (count === data.info.issueCount) {
			const infoCount = await this.insertInfo(data.info);
		}
	}

	async checkInfo(info) {
		const client = await MongoClient.connect(dburl);
		const collection = client.db(this.dbName).collection(statusCollectionName);
		const statusCount = await collection.countDocuments({$and: [{issueNumber: info.month}, {issueType: info.type}]})
		// console.log(`statusCount: ${statusCount} ${info.month} ${info.type}`)
		return statusCount >= 1;
	}

	async downloadPdf(info) {
		return new Promise((resolve, reject) => {
			const cacheDir = path.resolve(process.cwd(), 'cache')
			if (!fs.existsSync(cacheDir)) {
				fs.mkdirSync(cacheDir);
			}
			let total = 0;
			let progress = 0;
			const filepath = `cache/${this.cityName}-${info.month}-${info.type}-${new Date().valueOf()}.pdf`
			request(info.link)
				.on('error', function (err) {
					console.error(err);
					reject(err);
				})
				.on('response', function (response) {
					// console.log(response.statusCode) // 200
					// console.log(response.headers['content-type']) // 'application/pdf'
					total = response.headers['content-length'];
				})
				.on('data', function (data) {
					// decompressed data as it is received
					progress += data.length;
					if (progress >= total) {
						resolve(filepath)
					}
				})
				.pipe(fs.createWriteStream(filepath))
		})
	}

	async getPdfContent(file) {
		return new Promise((resolve, reject) => {
			const pdfParser = new PDFParser(this, 1);
			pdfParser.on("pdfParser_dataError", error => {
				console.error(error)
				reject(error)
			});
			pdfParser.on("pdfParser_dataReady", pdfData => {
				// console.log(pdfData)
				const content = pdfParser.getRawTextContent();
				resolve(content)
			});

			pdfParser.loadPDF(file);
		})
	}

// {
//      issues: [{
//          number: 1,
//          code: 0808101526992,
//          name: '梁小燕',
//          type: '个人普通车' | '个人节能车' | '单位普通车' | '单位节能车',
//          month: '201810',
//      }],
//      info:{
//          issueType: '个人普通车' | '个人节能车' | '单位普通车' | '单位节能车',
//          issueNumber: '201810',
//          issueDesc: '2018年10月个人指标配置',
//          issueDate: '2018-10-26',
//          issueTime: '2018-10-26 10:14:54',
//          applyCount: 715923,
//          issueCount: 4500,
//          initValue: 539607
//      }
// }
	getDataFromContent(content, type, month) {
		const result = {
			info: {
				issueType: type,
			},
			issues: [], //
		};
		const lines = content.split('\r\n');
		for (const line of lines) {
			if (!line || !line.trim()) {
				continue;
			}
			if (line.includes('分期编号')) {
				result.info.issueNumber = this.getValueText(line);
				continue;
			}
			if (line.includes('分期描述')) {
				result.info.issueDesc = this.getValueText(line);
				continue;
			}
			if (line.includes('分期指标配置日期')) {
				result.info.issueDate = this.getValueText(line);
				continue;
			}
			if (line.includes('实际指标配置时间')) {
				result.info.issueTime = this.getValueText(line);
				continue;
			}
			if (line.includes('数据生成时间')) {
				result.info.issueTime = this.getValueText(line);
				continue;
			}
			if (line.includes('有效个人申请编码总数')) {
				result.info.applyCount = parseInt(this.getValueText(line), 10);
				continue;
			}
			if (line.includes('有效单位申请编码总数')) {
				result.info.applyCount = parseInt(this.getValueText(line), 10);
				continue;
			}
			if (line.includes('配置个人指标总数')) {
				result.info.issueCount = parseInt(this.getValueText(line), 10);
				continue;
			}
			if (line.includes('配置单位指标总数')) {
				result.info.issueCount = parseInt(this.getValueText(line), 10);
				continue;
			}
			if (line.includes('指标配置初始值')) {
				result.info.initValue = parseInt(this.getValueText(line), 10);
				continue;
			}
			// page break and page number line
			// ----------------Page (117) Break----------------
			// -119-
			if (line.includes('-')) {
				continue;
			}
			if (line.includes('中签详细列表数据完成')) {
				break;
			}
			if (line.match(/序号\s+申请编码\s+姓名/)) {
				continue;
			}
			if (line.match(/序号\s+申请编码\s+名称/)) {
				continue;
			}
			if (line.match(/序号\s+摇号基数序号\s+申请编码/)) {
				continue;
			}
			const match = line.match(/\S+/g);
			if (match && match.length === 3) {
				result.issues.push({
					number: match[0],
					code: match[1],
					name: match[2],
					type,
					month
				});
			}
		}


		return result;
	}

	async insertData(data) {
		const client = await MongoClient.connect(dburl);
		const collection = client.db(this.dbName).collection(resultsCollectionName);
		const result = await collection.insertMany(data)
		await client.close(true);
		return result.insertedCount;
	}

	async insertInfo(info) {
		const client = await MongoClient.connect(dburl);
		const collection = client.db(this.dbName).collection(statusCollectionName);
		const result = await collection.insertOne(info);
		await client.close(true);
		return result.insertedCount;
	}


	formatLink(link) {
		if (!link.startsWith('http')) {
			return 'http:' + link;
		} else {
			return link;
		}
	}

	getType(text) {
		if (text.includes('个人普通车')) {
			return '个人普通车';
		}
		if (text.includes('个人节能车')) {
			return '个人节能车';
		}
		if (text.includes('单位普通车')) {
			return '单位普通车';
		}
		if (text.includes('单位节能车')) {
			return '单位节能车';
		}
	}

	getValueText(text) {
		const split = text.split('：');
		if (split.length = 2) {
			return split[1];
		} else {
			return '';
		}
	}

}

module.exports = PdfService;
