const PdfService = require('./PdfService');

class GZ extends PdfService{
	constructor(){
		super();
		this.baseUrl = 'https://jtzl.gzjt.gov.cn/';
		this.dbName = 'gzyh';
		this.cityName = '广州';
	}

}

module.exports = new GZ();

