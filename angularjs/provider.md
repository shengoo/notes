# Provider

Providers are the only service you can pass into your .config() function. Use a provider when you want to provide module-wide configuration for your service object before making it available.

## Sample

```js
angular.module('app')
	.provider('sampleProvider', sampleProvider);

function sampleProvider(){

	/**
	 * Method on this can be called in config
	 */
	this.config = function(){

	}

	/*
	 * Methods returned from $get can be called in controllers.
	 */
	this.$get = function(){
		return{
			method:function(){
				return 'some value';
			}
		}
	}
}
```
