# Factory

When you’re using a Factory you create an object, add properties to it, then return that same object. When you pass this factory into your controller, those properties on the object will now be available in that controller through your factory.

## sample
```js
angular.module('app')
	.factory('myFactory', myFactory);

/**
 * return an object.
 */
function myFactory(){
	var service = {};
	service.method = function(){

	}
	return service;
}
```
