# Service

When you’re using Service, AngularJS instantiates it behind the scenes with the ‘new’ keyword. Because of that, you’ll add properties to ‘this’ and the service will return ‘this’. When you pass the service into your controller, those properties on ‘this’ will now be available on that controller through your service.

## Sample
```js
angular.module('app')
	.service('sampleService', sampleService);


/**
 * will return a object of this constructor.
 */
function sampleService(){
	this.method = function(){
		
	}
}
```
