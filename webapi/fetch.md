# fetch 用法简介

[Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)提供了一个获取资源的接口（包括跨域）。任何使用过 XMLHttpRequest 的人都能轻松上手，但新的API提供了更强大和灵活的功能集。

## 参数

第一个参数是URL地址。

第二个参数（可选）是fetch使用的选项（method、headers等选项）

### fetch选项

```js
fetch(url, {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    mode: "cors", // no-cors, cors, *same-origin
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    credentials: "same-origin", // include, *same-origin, omit
    headers: {
        "Content-Type": "application/json; charset=utf-8",
        // "Content-Type": "application/x-www-form-urlencoded",
    },
    redirect: "follow", // manual, *follow, error
    referrer: "no-referrer", // no-referrer, *client
    body: JSON.stringify(data), // body data type must match "Content-Type" header
})
```

## 返回结果

fetch会返回一个promise对象，resolve对应请求的Response。

### response属性：

1. body
1. bodyUsed

    一个布尔值来标示该Response是否读取过Body

1. headers

    此Response所关联的Headers 对象.

1. ok

    一个布尔值来标示该Response成功(状态码200-299) 还是失败.

1. redirected

    该Response是否来自一个重定向，如果是的话，它的URL列表将会有多个

1. status

    Response的状态码 (例如, 200 成功).

1. statusText

    与该Response状态码一致的状态信息 (例如, OK对应200).

1. type

    Response的类型 (例如, basic, cors).

1. url

    Response的URL.

### response方法

1. arrayBuffer()

    读取 Response对象并且将它设置为已读（因为Responses对象被设置为了 stream 的方式，所以它们只能被读取一次） ,并返回一个被解析为ArrayBuffer格式的promise对象

1. blob()

    读取 Response对象并且将它设置为已读（因为Responses对象被设置为了 stream 的方式，所以它们只能被读取一次） ,并返回一个被解析为Blob格式的promise对象

1. formData()

    读取Response对象并且将它设置为已读（因为Responses对象被设置为了 stream 的方式，所以它们只能被读取一次） ,并返回一个被解析为FormData格式的promise对象

1. json()

    读取 Response对象并且将它设置为已读（因为Responses对象被设置为了 stream 的方式，所以它们只能被读取一次） ,并返回一个被解析为JSON格式的promise对象

1. text()

    读取 Response对象并且将它设置为已读（因为Responses对象被设置为了 stream 的方式，所以它们只能被读取一次） ,并返回一个被解析为USVString格式的promise对象

### 示例：读取一个图片并生成URL

```js
var myImage = document.querySelector('.my-image');
fetch('flowers.jpg').then(function(response) {
  return response.blob();
}).then(function(response) {
  var objectURL = URL.createObjectURL(response);
  myImage.src = objectURL;
});
```

## get请求

```js
fetch('http://example.com/movies.json')
  .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
    console.log(myJson);
  });
```

## post请求

post json

```js
fetch(url, {
  method: 'POST',
  body: JSON.stringify(data),
  headers: new Headers({
    'Content-Type': 'application/json'
  })
})
```

### 上传文件&提交表单

```js
var formData = new FormData();
var fileField = document.querySelector("input[type='file']");

formData.append('username', 'abc123');
formData.append('avatar', fileField.files[0]);

fetch('https://example.com/upload', {
  method: 'PUT',
  body: formData
})
.then(response => response.json())
.catch(error => console.error('Error:', error))
.then(response => console.log('Success:', response));
```
