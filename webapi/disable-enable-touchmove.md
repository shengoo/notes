# 移动端禁用和启用滚动

弹窗和弹框是网站应用中经常用到的技术，但是在页面弹出一个全屏的弹窗的时候，还是可以使用触摸来滑动弹窗下面的页面。

<img src="https://www.sheng00.com/wp-content/uploads/2018/06/ezgif-4-073e484c60.gif" alt="" width="480" height="1029" class="aligncenter size-full wp-image-2748" />

为了用户有更好的体验，可以使用以下代码，在弹窗的时候禁用滚动，弹窗关闭的时候再次启用滚动。

```js
// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove  = preventDefault; // mobile
    document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}
```
