
## negtive margin
```css
.child {
    position: relative;
}
.center{
    width: 300px;
    height: 200px;
    left: 50%;
    top: 50%;
    margin-left: -150px;
    margin-top: -100px;
    position: absolute;
}
```

## auto margin
```css
.parent {
    position: relative;
}
.child{
    width: 400px;
    height: 300px;
    margin: auto;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
}
```

## translate
```css
.parent {
    position: relative;
}
.child {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}
```

## flex

`align-items` vertical center

`justify-content` horitional center